import pyUni10 as uni10
import copy
import numpy as np
from numpy import linalg as LA
import MPSclass 
import cUD as UD
import matplotlib.pyplot as plt
import matplotlib
import pylab
from mpl_toolkits.mplot3d import axes3d

N_x=6
N_y=6

#No-symmetry
# D=[2]
# chi_boundry=[6]
# chi_single=[8]
# chi_try=[2]
# d_in=[2]
# d_out=[8]

#Z2-symmetry
D=[2,2]
chi_boundry=[20]
chi_single=[20]
chi_try=[10]
d_in=[1,1]
d_out=[1,1]

#U1-symmetry
#D=[1,2,1,1]
#chi_boundry=[200]
#chi_single=[200]
#chi_try=[20]
#d_phys=[1,1]

interval=+1.0e-2
threshold=+1.0e+1
accuracy=+1.0e-8
Model=["Fer_Z2","off"]               #ITF,ITF_Z2, Heis, Heis_Z2, Heis_U1, Fer_Z2, Fer_U1#
N_iter_total=10
N_tebd=10
h_coupling=[1.0, 2.0]
Sys='Fer'                 #Fer, Bos
start_itebd=1.0
division_itebd=5.0
N_iter_SU=[20, "full"]     #"full" or "QR"


print Model, Sys, "h=", h_coupling, "D=", D, "chi_single", chi_single


lat_sp=1.0/N_x
mass=0.5
mu_0=-3.0
V_0=-4.0
g_val=1.5000
Per_cos=1              #periodicity



PEPS_mps=[None]*(N_x/2)
PEPS_mps_left=[None]*(N_x/2)
PEPS_mps_right=[None]*(N_x/2)
PEPS_listten=[None]*(N_x/2)

PEPS_mps_leftU=[None]*(N_x/2)
PEPS_mps_rightU=[None]*(N_x/2)
PEPS_listtenU=[None]*(N_x/2)

mps_boundry_left=[None]*(N_x/2)
mps_boundry_right=[None]*(N_x/2)
mps_boundry_temp=[None]*(N_x/2)

q_D, q_chi_boundry, q_chi_single, q_chi_try, q_d_in, q_d_out=UD.full_make_bond( Model, D, chi_boundry, chi_single, chi_try, d_in, d_out)

#print q_d_in, q_d_out

for i in xrange(N_x/2):
  PEPS_listten[i]=UD.Init_PEPS( N_x/2, q_D, q_d_out, i)
  PEPS_listtenU[i]=UD.Init_PEPS( N_x/2, q_D, q_d_out, i)

PEPS_listten, norm_val, count_val=UD.Normalize_PEPS( PEPS_listten, N_x/2, q_D, q_chi_try, q_d_out, threshold, interval,Sys)

Q_list=UD.Init_Q_list( N_x/2, q_d_in, q_d_out)
H_col=UD.make_H_col( N_x, g_val, q_d_in, lat_sp, mu_0, mass, V_0, Per_cos, h_coupling, Model)
H_row=UD.make_H_row( N_x, g_val, q_d_in, lat_sp, mu_0, mass, V_0, Per_cos, h_coupling, Model)


#print H_col[1][1]
#print H_row[1][1]

HA_col=UD.Ascend_f_col( Q_list, H_col, N_x/2, Sys)
HA_row=UD.Ascend_f_row( Q_list, H_row, N_x/2, Sys)



#UD.Store_f(PEPS_listten, N_x)

UD.Reload_f( PEPS_listten, N_x/2)
UD.Reload_Q_list( Q_list, N_x/2)
#UD.increase_physicalbond( Q_list, PEPS_listten, N_x/2, q_d_out)


Mag_f_list=[]
E_f_list=[]
h_list=[]
E_iter_list=[]
E_iter_list1=[]
count_list=[]



Fidel_val=1
E_coulmn=[]
E_row=[]
E_mag_coulmn=[]
E_mag_row=[]
E_0=1.0
E_1=1.0

E_00=1.0
E_11=1.0

Landa_col=UD.Landa_f_col( q_D, N_x/2)
Landa_row=UD.Landa_f_row( q_D, N_x/2)
UD.Store_Gamma( PEPS_listten, N_x/2)
UD.Store_Landa_row( Landa_row, N_x/2)
UD.Store_Landa_col( Landa_col, N_x/2)




for iter in xrange(N_iter_total):
 print iter
 E_min=1
 E_0=1
 E_1=100
 count_iter=0


 UD.Q_update( H_col, H_row, N_x/2, PEPS_listten, E_iter_list, q_D, accuracy, q_d_out, q_chi_single, q_chi_try, Q_list,Sys)

 HA_col=UD.Ascend_f_col( Q_list, H_col, N_x/2,Sys)
 HA_row=UD.Ascend_f_row( Q_list, H_row, N_x/2,Sys)
 #rho_row, rho_col=UD.make_density_matrix_sinlgeLayer( PEPS_listten, N_x/2, q_chi_single, q_d_out, q_D,Sys)

 rho_row, rho_col=UD.make_density_matrix_double( PEPS_listten, N_x/2, q_chi_single, q_d_out, q_D,Sys)

 E_0=UD.energy_from_Density( rho_row, rho_col, HA_col, HA_row,N_x/2)
 print "E_f",E_0




# if iter==0:
#  UD.Reload_Landa_row(Landa_row, N_x/2)
#  UD.Reload_Landa_col(Landa_col, N_x/2)
#  UD.Reload_Gamma(PEPS_listten, N_x/2)

#  PEPS_listten, Landa_col, Landa_row=UD.simple_update( PEPS_listten, Landa_col, Landa_row, start_itebd, division_itebd, N_iter_SU, HA_col, HA_row, q_d_out, Model, N_x/2, q_D,q_chi_try, threshold, interval,Sys)

#  UD.Store_Gamma( PEPS_listten, N_x/2)
#  UD.Store_Landa_row( Landa_row, N_x/2)
#  UD.Store_Landa_col( Landa_col, N_x/2)
#  PEPS_listten=UD.make_PEPS_tensors(PEPS_listten, Landa_row, Landa_col,N_x/2)


 #UD.TEBD_Full(HA_col, HA_row, N_x/2, PEPS_listten, E_iter_list, q_D, accuracy, N_tebd, i, q_d_out, q_chi_single, q_chi_try, mps_boundry_temp, mps_boundry_left, mps_boundry_right, threshold, interval, Model,count_list,E_iter_list1,Sys)

 UD.TEBD_Full_double(HA_col, HA_row, N_x/2, PEPS_listten, E_iter_list, q_D, accuracy, N_tebd, i, q_d_out, q_chi_boundry, q_chi_try, mps_boundry_temp, mps_boundry_left, mps_boundry_right, threshold, interval, Model,count_list,E_iter_list1,Sys)




file = open("Energy.txt", "w")
for index in range(len(E_iter_list)):
  file.write(str(index) + " " + str(E_iter_list[index]/(N_x*N_x))+" "+ "\n")
file.close()











