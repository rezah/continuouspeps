import pyUni10 as uni10
import copy
import numpy as np
from numpy import linalg as LA
import MPSclass 
import UniformDisentangler as UD
N_x=8
N_y=8
D=2
d=2
chi_boundry=20
chi_single=20
chi_express=2
chi_try=20
interval=+1.0e-2
threshold=+1.0e+6
accuracy=+1.0e-6
Model="ITF"               #ITF, Heis#
N_iter_QR=25
N_iter_QR_first=45
z_coupling=1.0
j_coupling=3.50
Grid__coupling=0.00
N_coupling_probe=1


PEPS_mps=[None]*N_x
PEPS_mps_left=[None]*N_x
PEPS_mps_right=[None]*N_x
PEPS_listten=[None]*N_x

PEPS_mps_leftU=[None]*N_x
PEPS_mps_rightU=[None]*N_x
PEPS_listtenU=[None]*N_x

mps_boundry_left=[None]*N_x
mps_boundry_right=[None]*N_x
mps_boundry_temp=[None]*N_x

count_list=[]
Norm_list=[]
Norm_list_boundry=[]

for i in xrange(N_x):
  PEPS_mps[i]=UD.Init_PEPS( N_y, N_x, D, d,i)

for i in xrange(N_x):
  PEPS_listten[i]=UD.mps_to_tensor_left(PEPS_mps[i], N_x, N_y, D, d, i)
  PEPS_listtenU[i]=UD.mps_to_tensor_left(PEPS_mps[i], N_x, N_y, D, d, i)

#UD.Store_f(PEPS_listten, N_x, N_y)
#UD.Reload_f(PEPS_listten, N_x, N_y)
#PEPS_listten=UD.increase_bond(PEPS_listten,D, N_x, N_y)


for Location in xrange(N_x-1):
  mps_boundry_left[Location]=UD.make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_single, N_x)

for Location in xrange(N_x-1):
  PEPS_ten=UD.rotate(PEPS_listten[N_x-Location-1])
  mps_boundry_temp[Location]=UD.make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_x)
  mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])

norm_val=1 
for i in xrange(N_x-1):
  print "Norm:single_Layer=", i, mps_boundry_left[i].product(mps_boundry_right[i+1])
  norm_val=mps_boundry_left[i].product(mps_boundry_right[i+1])
 





PEPS_listten, norm_val, count_val=UD.Normalize_PEPS(PEPS_listten, N_x, N_y, D, chi_try, d, threshold, interval)




# for Location in xrange(N_x):
#   mps_boundry_left[Location]=UD.make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_single, N_x)
# 
# for Location in xrange(N_x):
#   PEPS_ten=UD.rotate(PEPS_listten[N_x-Location-1])
#   mps_boundry_temp[Location]=UD.make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_x)
#   mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])
# #
# norm_val=1 
# for i in xrange(N_x-1):
#   print "Norm:singleLayer=", i, mps_boundry_left[i].product(mps_boundry_right[i+1])
#   norm_val=mps_boundry_left[i].product(mps_boundry_right[i+1])
 




# #print PEPS_listten[0][1].printDiagram()



# for Location in xrange(N_x):
#  mps_boundry_left[Location]=UD.make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_single, N_x)
# 
# 
# for Location in xrange(N_x):
#  PEPS_ten=UD.rotate(PEPS_listten[N_x-Location-1])
#  mps_boundry_temp[Location]=UD.make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_x)
#  mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])

#MPO_Ten=UD.make_boundry_MPO(PEPS_listten, N_y, N_x)
#Env_left, Env_right=UD.make_ENVMPS(MPO_Ten, N_y, N_x, chi_boundry)
#Norm_val_boundry=Env_left[0].product(Env_right[1])

#for i in xrange(N_x-1):
 #print "Norm:boundryMethod=", i, Env_left[i].product(Env_right[i+1]), mps_boundry_left[i].product(mps_boundry_right[i+1])

# for i in xrange(N_x-1):
#  print "Norm:boundryMethod=", i, mps_boundry_left[i].product(mps_boundry_right[i+1])


# for i in xrange(N_x):
#  PEPS_mps_left[i]=UD.tensor_to_mps_left(PEPS_listten[i], N_x, N_y)
# for i in xrange(N_x):
#  PEPS_mps_right[i]=UD.tensor_to_mps_right(PEPS_listten[i], N_x, N_y)



Mag_f_list=[]
E_f_list=[]
h_list=[]
E_iter_list=[]
Q_norm_list=[]
N_iter_list=[]
Trun_list=[]

Fidel_val=1
E_coulmn=[]
E_row=[]
E_mag_coulmn=[]
E_mag_row=[]
E_0=1.0
E_1=1.0


mps_I=MPSclass.MPS(1,1,2*N_y)
for i_ind in xrange(mps_I.N):
 mps_I[i_ind].identity()



for x_coupling in xrange(N_coupling_probe):

 if x_coupling==0:
  j_coupling=j_coupling
 else:
  j_coupling=j_coupling+Grid__coupling

 h_coupling=[z_coupling, j_coupling]

 #if x_coupling==0:
  #h_coupling=[0, 1]

 if x_coupling==0:
  List_delN=UD.Short_TrotterSteps_start(N_iter_QR_first)
 else:
  List_delN=UD.Short_TrotterSteps(N_iter_QR)

 print h_coupling, List_delN

 E_min=1
 E_0=1
 E_1=1000
 count_iter=0
 for delta, N_iter in List_delN:
  print delta, N_iter

  H=UD.Heisenberg(h_coupling, d, Model)
  U_ham = uni10.UniTensor(H.bond(), "U_ham")
  U_ham.putBlock(uni10.takeExp(-delta, H.getBlock()))

  H0=UD.Heisenberg0(h_coupling, d, Model)
  U_ham0 = uni10.UniTensor(H0.bond(), "U_ham0")
  U_ham0.putBlock(uni10.takeExp(-delta, H0.getBlock()))

  HN=UD.HeisenbergN(h_coupling, d, Model)
  U_hamN = uni10.UniTensor(HN.bond(), "U_hamN")
  U_hamN.putBlock(uni10.takeExp(-delta, HN.getBlock()))


  m_mag=UD.Mag(d)
  m_mag0=UD.Mag0(d)
  m_magN=UD.MagN(d)

  Energy_val,E_c, E_r=UD.Energy_cal(PEPS_listten, d, chi_single, mps_boundry_temp, mps_boundry_left, mps_boundry_right, N_x, N_y, D,U_ham0, U_ham, U_hamN, H0, H, HN)
  E_0=Energy_val
  print "E_0", Energy_val


  for q_iter in xrange(N_iter):



###########################   Col #############################


###   make_right_Env   ######
   for Location in xrange(N_x-1):
    PEPS_ten=UD.rotate(PEPS_listten[N_x-Location-1])
    mps_boundry_temp[Location]=UD.make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_x)
    mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])
#############

   E_coulmn_t=[]
   for i_ind in xrange(N_x):
    print "N_x", i_ind
    if i_ind==0:
     UD.update_twotensor(mps_I, mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_x, N_y, U_ham0, U_ham, U_hamN, H0, H, HN, D, E_coulmn_t)
    elif i_ind==N_x-1:
     UD.update_twotensor(mps_boundry_left[i_ind-1], mps_I, PEPS_listten[i_ind], N_x, N_y,U_ham0, U_ham, U_hamN, H0, H, HN, D, E_coulmn_t)
    else:
     UD.update_twotensor(mps_boundry_left[i_ind-1], mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_x, N_y,U_ham0, U_ham, U_hamN, H0, H, HN, D, E_coulmn_t)

 

    if  i_ind==(N_x-1):break
    mps_boundry_left[i_ind]=UD.make_Env_singleLayer(PEPS_listten[i_ind], i_ind, mps_boundry_left[i_ind-1], d, chi_single, N_x)
    norm_val=mps_boundry_left[i_ind].product(mps_boundry_right[i_ind+1])
    if norm_val>threshold or norm_val<(1.0/threshold):
     print "Warning", norm_val
     PEPS_listten, norm_val, count_val=UD.Normalize_PEPS(PEPS_listten, N_x, N_y, D, chi_try, d, threshold, interval)
     print "Fixed", norm_val, "Num_iter", count_val
     mps_boundry_right, mps_boundry_left=UD.update_env_LR(PEPS_listten, N_x, d, chi_single, N_y, mps_boundry_left, mps_boundry_right, mps_boundry_temp)


##########################   Row   ###############################
   PEPS_listten=UD.rotate_all(PEPS_listten, N_x, N_y)

   for Location in xrange(N_x-1):
    PEPS_ten=UD.rotate(PEPS_listten[N_x-Location-1])
    mps_boundry_temp[Location]=UD.make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_y)
    mps_boundry_right[N_y-Location-1]=copy.copy(mps_boundry_temp[Location])


   E_row_t=[]
   for i_ind in xrange(N_y):
    print "N_y", i_ind
    if i_ind==0:
     UD.update_twotensor(mps_I, mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_y, N_x, U_ham0, U_ham, U_hamN, H0, H, HN, D, E_row_t)
    elif i_ind==N_x-1:
     UD.update_twotensor(mps_boundry_left[i_ind-1], mps_I, PEPS_listten[i_ind], N_y, N_x,U_ham0, U_ham, U_hamN, H0, H, HN, D, E_row_t)
    else:
     UD.update_twotensor(mps_boundry_left[i_ind-1], mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_x, N_y, U_ham0, U_ham, U_hamN, H0, H, HN, D, E_row_t)


    if  i_ind==(N_y-1):break
    mps_boundry_left[i_ind]=UD.make_Env_singleLayer(PEPS_listten[i_ind], i_ind, mps_boundry_left[i_ind-1], d, chi_single, N_y)
    norm_val=mps_boundry_left[i_ind].product(mps_boundry_right[i_ind+1])
    if norm_val>threshold or norm_val<(1.0/threshold):
     print "Warning", norm_val
     PEPS_listten, norm_val, count_val=UD.Normalize_PEPS(PEPS_listten, N_x, N_y, D, chi_try, d, threshold, interval)
     print "Fixed", norm_val, count_val
     mps_boundry_right, mps_boundry_left=UD.update_env_LR(PEPS_listten, N_x, d, chi_single, N_y, mps_boundry_left, mps_boundry_right, mps_boundry_temp)

   PEPS_listten=UD.rotate_all(PEPS_listten, N_x, N_y)


#### Cal Energy ####
   Energy_val,E_c, E_r=UD.Energy_cal(PEPS_listten, d, chi_single, mps_boundry_temp, mps_boundry_left, mps_boundry_right, N_x, N_y, D,U_ham0, U_ham, U_hamN, H0, H, HN)

   E_0=E_1*1.0
   E_1=Energy_val
   print E_1, E_0, (E_1-E_0)/E_1 
   if E_1 < E_0:
    UD.Store_f(PEPS_listten, N_x, N_y)
    PEPS_listtenU=UD.copy_f(PEPS_listten,N_x, N_y, PEPS_listtenU)



   if ((E_1-E_0)/E_1)<accuracy or E_1>E_0:
    if ((E_1-E_0)/E_1)<accuracy:print "loop_finished:reached_levelofaccuracy"
    else: print "Not_get_lowered";
    PEPS_listten=UD.copy_f(PEPS_listtenU,N_x, N_y, PEPS_listten)
    
    break















# h_list.append(h_coupling[1])
# #Mag_f_list.append(Mag_f)
# E_f_list.append(E_1)



#file = open("Q_norm.txt", "w")
#for index in range(len(N_iter_list)):
#  file.write(str(index) + " " + str(Q_norm_list[index])+" "+ "\n")
#file.close()


#file = open("EnergyIter.txt", "w")
#for index in range(len(N_iter_list)):
#  file.write(str(N_iter_list[index]) + " " + str(E_iter_list[index])+" "+ "\n")
#file.close()



#file = open("Trun.txt", "w")
#for index in range(len(Trun_list)):
#  file.write(str(index) + " " + str(Trun_list[index])+" "+ "\n")
#file.close()


##file = open("Mag.txt", "w")
##for index in range(len(h_list)):
##  file.write(str(h_list[index]) + " " + str(Mag_f_list[index])+" "+ "\n")
##file.close()



#file = open("Energy.txt", "w")
#for index in range(len(h_list)):
#  file.write(str(h_list[index]) + " " + str(E_f_list[index])+" "+ "\n")
#file.close()



