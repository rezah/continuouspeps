import pyUni10 as uni10
import copy
import numpy as np
import scipy as sp
#from numpy import linalg as LA
from scipy import linalg as LA
import MPSclass 


def Short_TrotterSteps_start(N_iterF):
 List_delN=[]

 #Delta_N=(1.0, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.2, N_iterF)
 #List_delN.append(Delta_N)


 #Delta_N=(0.08, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.07, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.05, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.04, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.03, N_iterF)
 #List_delN.append(Delta_N)

 Delta_N=(0.02, N_iterF)
 List_delN.append(Delta_N)

 #Delta_N=(0.01, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.009, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.005, N_iterF)
 #List_delN.append(Delta_N)



 #Delta_N=(0.0001, N_iterF)
 #List_delN.append(Delta_N)


 #Delta_N=(0.000001, N_iterF)
 #List_delN.append(Delta_N)

 return List_delN




def Short_TrotterSteps(N_iterF):
 List_delN=[]

 #Delta_N=(1.0, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.08, N_iterF)
 #List_delN.append(Delta_N)


 #Delta_N=(0.07, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.06, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.05, N_iterF)
 #List_delN.append(Delta_N)

 Delta_N=(0.04, N_iterF)
 List_delN.append(Delta_N)

 #Delta_N=(0.03, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.02, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.01, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.009, N_iterF)
 #List_delN.append(Delta_N)

# Delta_N=(0.008, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.007, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.006, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.005, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.004, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.003, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.002, N_iterF)
# List_delN.append(Delta_N)

# for i in xrange(5, 1, -1):
#  Delta_N=(i*(1.0/10),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(10, 1, -1):
#  Delta_N=(i*(1.0/100),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(5, 5, -1):
#  Delta_N=(i*(1.0/100),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(10, 1, -1):
#  Delta_N=(i*(1.0/1000),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(10, 0, -1):
#  Delta_N=(i*(1.0/10000),N_iterF)
#  List_delN.append(Delta_N)

 return List_delN

def increase_bond( PEPS_listten, D, N_x, N_y):

  bdiD=uni10.Bond(uni10.BD_IN, D)
  bdoD=uni10.Bond(uni10.BD_OUT, D)


  T0=uni10.UniTensor([bdiD, PEPS_listten[1][1].bond(3)])
  T0.randomize()
  svd=T0.getBlock().svd()
  T0.putBlock(svd[0])
  #T0.identity()
  T1=copy.copy(T0)
  T0.setLabel([1,0])
  #T1.setLabel([1,2])
  #result=T0*T1
  #result.permute([0,2],1)
  #print result

  for i in xrange(N_x):
   for j in xrange(N_y):
     if i==0 and j==0:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-3,3])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-4,4])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([0,1,2,-3,-4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif i==0 and j==N_y-1:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-3,3])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-1,1])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([0,-1,2,-3,4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif i==N_x-1 and j==0:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-100,0])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-4,4])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([-100,1,2,3,-4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif i==N_x-1 and j==N_y-1:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-100,0])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-1,1])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([-100,-1,2,3,4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif j==0 and i>0 and i<N_x-1:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-100,0])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
#      T0.setLabel([-1,1])
#      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-3,3])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-4,4])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([-100,1,2,-3,-4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif j==N_y-1 and i>0 and i<N_x-1:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-100,0])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-1,1])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-3,3])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
#      T0.setLabel([-4,4])
#      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([-100,-1,2,-3,4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif i==0 and j>0 and j<N_y-1:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-1,1])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
#      T0.setLabel([-1,1])
#      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-3,3])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-4,4])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([0,-1,2,-3,-4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     elif i==N_x-1 and j>0 and j<N_y-1:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-100,0])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-1,1])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-4,4])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
#      T0.setLabel([-4,4])
#      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([-100,-1,2,3,-4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
     else:
      #print i, j
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      T0.setLabel([-100,0])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-1,1])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-3,3])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      T0.setLabel([-4,4])
      PEPS_listten[i][j]=PEPS_listten[i][j]*T0
      PEPS_listten[i][j].permute([-100,-1,2,-3,-4],3)
      PEPS_listten[i][j].setLabel([0,1,2,3,4])
      
      
  return  PEPS_listten




def Store_f(PEPS_listten, N_x, N_y):
 for i in xrange(N_x):
  for j in xrange(N_y):
   PEPS_listten[i][j].save("Store/a" + str(i)+str(j))

def Reload_f(PEPS_listten, N_x, N_y):
 for i in xrange(N_x):
  for j in xrange(N_y):
   PEPS_listten[i][j]=uni10.UniTensor("Store/a" + str(i)+str(j))


def   copy_f(PEPS_listten,N_x, N_y, PEPS_listtenU):
 for i in xrange(N_x):
  for j in xrange(N_y):
   PEPS_listtenU[i][j]=PEPS_listten[i][j]*1.0

 return  PEPS_listtenU




def Energy_cal(PEPS_listten, d, chi_single, mps_boundry_temp, mps_boundry_left, mps_boundry_right, N_x, N_y, D, U_ham0, U_ham, U_hamN, H0, H, HN):
   mps_I=MPSclass.MPS(1,1,2*N_y)
   for i_ind in xrange(mps_I.N):
    mps_I[i_ind].identity()

##################   Col   #####################

   for Location in xrange(N_x-1):
    PEPS_ten=rotate(PEPS_listten[N_x-Location-1])
    mps_boundry_temp[Location]=make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_x)
    mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])

   for Location in xrange(N_x-1):
     mps_boundry_left[Location]=make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_single, N_x)

   E_coulmn_t=[]
   for i_ind in xrange(N_x):
    if i_ind==0:
     update_twotensor_energy(mps_I, mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_x, N_y, U_ham0, U_ham, U_hamN, H0, H, HN, D, E_coulmn_t)
    elif i_ind==N_x-1:
     update_twotensor_energy(mps_boundry_left[i_ind-1], mps_I, PEPS_listten[i_ind], N_x, N_y,U_ham0, U_ham, U_hamN, H0, H, HN, D, E_coulmn_t)
    else:
     update_twotensor_energy(mps_boundry_left[i_ind-1], mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_x, N_y,U_ham0, U_ham, U_hamN, H0, H, HN, D, E_coulmn_t)

    if  i_ind==(N_x-1):break


   E_c=sum(E_coulmn_t)
   E_coulmn=[ E_coulmn_t[i]*1.0   for i in xrange(len(E_coulmn_t))  ]



#################   Row   #################
   PEPS_listten=rotate_all(PEPS_listten, N_x, N_y)

   for Location in xrange(N_x-1):
    PEPS_ten=rotate(PEPS_listten[N_x-Location-1])
    mps_boundry_temp[Location]=make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_y)
    mps_boundry_right[N_y-Location-1]=copy.copy(mps_boundry_temp[Location])

   for Location in xrange(N_x-1):
     mps_boundry_left[Location]=make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_single, N_x)

   E_row_t=[]
   for i_ind in xrange(N_y):
    if i_ind==0:
     update_twotensor_energy(mps_I, mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_y, N_x, U_ham0, U_ham, U_hamN, H0, H, HN, D, E_row_t)
    elif i_ind==N_x-1:
     update_twotensor_energy(mps_boundry_left[i_ind-1], mps_I, PEPS_listten[i_ind], N_y, N_x,U_ham0, U_ham, U_hamN, H0, H, HN, D, E_row_t)
    else:
     update_twotensor_energy(mps_boundry_left[i_ind-1], mps_boundry_right[i_ind+1], PEPS_listten[i_ind], N_x, N_y, U_ham0, U_ham, U_hamN, H0, H, HN, D, E_row_t)


    if  i_ind==(N_y-1):break

   E_r=sum(E_row_t)
   #print "E=", E_r, E_c, (E_c+E_r)/(N_x*N_y)
   E_row=[ E_row_t[i]*1.0   for i in xrange(len(E_row_t))  ]
   #E_0=E_1*1.0
   E_1=(E_c+E_r)/(N_x*N_y)
   
   
   
   
   PEPS_listten=rotate_all(PEPS_listten, N_x, N_y)


   return E_1, E_c, E_r



def  update_env_LR(PEPS_listten, N_x, d, chi_single, N_y,mps_boundry_left, mps_boundry_right, mps_boundry_temp):
 for Location in xrange(N_x-1):
  PEPS_ten=rotate(PEPS_listten[N_x-Location-1])
  mps_boundry_temp[Location]=make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_single,N_x)
  mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])
  for Location in xrange(N_x-1):
   mps_boundry_left[Location]=make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_single, N_x)


 return mps_boundry_right, mps_boundry_left



def Normalize_PEPS(PEPS_listten, N_x, N_y, D, chi_try, d, threshold, interval):

 norm_val=Cal_norm(PEPS_listten, N_x, N_y, D, chi_try, d)
 print "Zero_order",  norm_val

 count=0

 while count<100:
  if norm_val<threshold and norm_val>(1.0/threshold):break
  count=count+1
  alpha=1.0-interval
  if norm_val>threshold:
   for i in xrange(N_x):
    for j in xrange(N_y):
     PEPS_listten[i][j]=PEPS_listten[i][j]*alpha
   norm_val=Cal_norm(PEPS_listten, N_x, N_y, D, chi_try, d)

  alpha=1.0+interval
  if norm_val<(1.0/threshold):
   for i in xrange(N_x):
    for j in xrange(N_y):
     PEPS_listten[i][j]=PEPS_listten[i][j]*alpha
   norm_val=Cal_norm(PEPS_listten, N_x, N_y, D, chi_try, d)

 PEPS_listten=All_dist(PEPS_listten,N_x, N_y, D)

 return PEPS_listten, norm_val, count


def  Cal_norm(PEPS_listten, N_x, N_y, D, chi_try, d):

 mps_boundry_left=[None]*N_x
 mps_boundry_right=[None]*N_x
 mps_boundry_temp=[None]*N_x

 for Location in xrange(N_x-1):
  mps_boundry_left[Location]=make_Env_singleLayer(PEPS_listten[Location], Location, mps_boundry_left[Location-1], d, chi_try, N_x)

 Location=0
 PEPS_ten=rotate(PEPS_listten[N_x-Location-1])
 mps_boundry_temp[Location]=make_Env_singleLayer(PEPS_ten, Location, mps_boundry_temp[Location-1],d,chi_try,N_x)
 mps_boundry_right[N_x-Location-1]=copy.copy(mps_boundry_temp[Location])

 norm_val=mps_boundry_left[N_x-2].product(mps_boundry_right[N_x-1])

 return   norm_val






def MaxAbs(c):
 blk_qnums = c.blockQnum()
 max_list=[]
 for qnum in blk_qnums:
    c_mat=c.getBlock(qnum)
    max_list.append(c_mat.absMax())
 max_list_f=[abs(x) for x in max_list]
 return max(max_list_f)

def max_ten(a):
 Max_val=MaxAbs(a)
 if ( Max_val < 0.5e-1) or (Max_val > 0.5e+1)   :

  if Max_val >= 1:
   print ">1",Max_val
   a=a*(1.00/Max_val)
  if Max_val < 1: 
   print "<1",Max_val
   a=a*(1.00/Max_val)

#  if Max_val >= 1:
#   print ">1",Max_val, Max_val**(1./2.)
#   a=a*(1.00/(Max_val**(1./2.)))
#  if Max_val < 1: 
#   print "<1",Max_val
#   a=a*(1.00/Max_val)

 else: a=a;
 return a

def All_dist(PEPS_listten,N_x, N_y, D):

 for i in xrange(N_x-1):
  for j in xrange(N_y):
   PEPS_listten[i][j], PEPS_listten[i+1][j]=equall_dis_H(PEPS_listten[i][j], PEPS_listten[i+1][j],D)
   PEPS_listten[i][j]=max_ten(PEPS_listten[i][j])
   PEPS_listten[i+1][j]=max_ten(PEPS_listten[i+1][j])
 
 for i in xrange(N_x):
  for j in xrange(N_y-1):
   PEPS_listten[i][j], PEPS_listten[i][j+1]=equall_dis_V(PEPS_listten[i][j], PEPS_listten[i][j+1],D)
   PEPS_listten[i][j]=max_ten(PEPS_listten[i][j])
   PEPS_listten[i][j+1]=max_ten(PEPS_listten[i][j+1])
 
 
 
 return PEPS_listten

def equall_dis_H(PEPS_1, PEPS_2, D):

 A=copy.copy(PEPS_1)
 A.setLabel([1,2,3,4,5])
 A.permute([1,2,5,3,4],3)
 
 #q, r= qr_parity1(A) 
 q,s,V=svd_parity5(A)
 V.setLabel([0,3,4])
 s.setLabel([-100,0])
 V=s*V
 V.permute([-100,3,4],2)
 r=V*1.0
 r.setLabel([-100,3,4])
 q.setLabel([1,2,5,-100])



 A=copy.copy(PEPS_2)
 A.setLabel([4,5,6,7,8])
 A.permute([4, 6, 5,8,7],2)
 U,s,qq=svd_parity6(A) 
 U.setLabel([4,6,0])
 s.setLabel([0,-200])
 U=U*s
 U.permute([4,6,-200],2)
 l=U*1.0
 qq.setLabel([-200,5,8,7])
 l.setLabel([4,6,-200])

 Teta=l*r
 Teta.permute([-100,3,-200,6],2)
 U, V, s= setTruncation3(Teta, D)

 #U,s,V=svd_parity2(Teta)

 U.setLabel([-100,3,17])
 s.setLabel([17,-17])
 V.setLabel([-17,-200,6])
 s=Sqrt(s)
 s.setLabel([17,-17])
 
 U=U*s
 V=s*V

 U.permute([-100,3,-17],1)
 U.setLabel([-100,3,4])
 V.permute([17,-200,6],1)
 V.setLabel([4,-200,6])
 
 PEPS_1=q*U
 PEPS_2=qq*V

 PEPS_1.permute([1,2,3,4,5],3)
 PEPS_2.permute([4,5,6,7,8],3)

 return PEPS_1, PEPS_2


def equall_dis_V(PEPS_1, PEPS_2, D):

 A=copy.copy(PEPS_1)
 A.setLabel([1,2,3,4,5])
 A.permute([1,2,4,3,5],3)
 
 #q, r= qr_parity1(A) 
 q,s,V=svd_parity5(A)
 V.setLabel([0,3,5])
 s.setLabel([-100,0])
 V=s*V
 V.permute([-100,3,5],2)
 r=V*1.0
 r.setLabel([-100,3,5])
 q.setLabel([1,2,4,-100])



 A=copy.copy(PEPS_2)
 A.setLabel([6,5,7,8,9])
 A.permute([5, 7, 6,8,9],2)
 U,s,qq=svd_parity6(A) 
 U.setLabel([5,7,0])
 s.setLabel([0,-200])
 U=U*s
 U.permute([5,7,-200],2)
 l=U*1.0
 qq.setLabel([-200,6,8,9])
 l.setLabel([5,7,-200])

 Teta=l*r
 Teta.permute([-100,3,-200,7],2)
 U, V, s= setTruncation3(Teta, D)

 #U,s,V=svd_parity2(Teta)

 U.setLabel([-100,3,17])
 s.setLabel([17,-17])
 V.setLabel([-17,-200,7])
 s=Sqrt(s)
 s.setLabel([17,-17])
 
 U=U*s
 V=s*V

 U.permute([-100,3,-17],1)
 U.setLabel([-100,3,5])
 V.permute([17,-200,7],1)
 V.setLabel([5,-200,7])
 
 PEPS_1=q*U
 PEPS_2=qq*V

 PEPS_1.permute([1,2,3,4,5],3)
 PEPS_2.permute([6,5,7,8,9],3)

 return PEPS_1, PEPS_2

def  norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H):
 val1=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 #print val1


 val2=(((r_up*r_d)*N_ten)*(l_up*l_d))*Ham
 #print val2
 return val1[0]-2*val2[0]

def  optimum_0(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H):
  
 Env_r=((N_ten)*(l_up*l_dp))*iden_h
 Env_r.permute([ -200,-3,-10, -100,3,10],3)

 Env_s=(((r_u)*N_ten)*(l_u*l_dp))*Ham
 #print Env_s.printDiagram()
 Env_s.permute([-200,-3,-10],2)

 U, S, V=svd_parityrl(Env_r)
 U.transpose()
 V.transpose()
 S=inverse(S)
 
 U.setLabel([7,8,9,10,11,12])
 S.setLabel([3,4,5,7,8,9])
 V.setLabel([0,1,2,3,4,5])

 A2_inv=V*S*U
 A2_inv.permute([0,1,2,10,11,12],3)
 A2_inv.setLabel([-100,3,10,-200,-3,-10])

 #distance_iden_val=distance_iden(A2_mat,A2_inv)
 #print 'distance1=', distance_iden_val
 #print A2.getBlock()*A2_inv

 r_up=A2_inv*Env_s
 r_up.permute([-100,3,10],3)
 r_up.setLabel([-100,3,10])
 r_dp=copy.copy(r_up)
 r_dp.setLabel([-200,-3,-10])

 return r_up, r_dp

def  optimum_1(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H):
  
 Env_r=((N_ten)*(r_up*r_dp))*iden_h
 Env_r.permute([-10,-13,-400,10,13,-300],3)

 Env_s=(((r_dp*r_u)*N_ten)*(l_u))*Ham
 #print Env_s.printDiagram()
 Env_s.permute([-10,-13,-400],2)

 U, S, V=svd_parityrl(Env_r)
 U.transpose()
 V.transpose()
 S=inverse(S)
 
 U.setLabel([7,8,9,10,11,12])
 S.setLabel([3,4,5,7,8,9])
 V.setLabel([0,1,2,3,4,5])

 A2_inv=V*S*U
 A2_inv.permute([0,1,2,10,11,12],3)
 A2_inv.setLabel([10,13,-300,-10,-13,-400])

 #distance_iden_val=distance_iden(A2_mat,A2_inv)
 #print 'distance1=', distance_iden_val
 #print A2.getBlock()*A2_inv

 l_up=A2_inv*Env_s
 l_up.permute([10,13,-300],3)
 #l_up.setLabel([3,6,-30])
 l_dp=copy.copy(l_up)
 l_dp.setLabel([-10,-13,-400])

 return l_up, l_dp



def  optimum_11(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H):
  
 Env_r=((N_ten)*(r_up*r_dp))*iden_h
 Env_r.permute([-10,13,-300,10,-13,-400],3)

 Env_s=(((r_dp*r_u)*N_ten)*(l_u))*Ham
 Env_s.permute([-10,13,-300],2)

 U, S, V=svd_parityrl(Env_r)
 U.transpose()
 V.transpose()
 S=inverse(S)
 
 U.setLabel([7,8,9,10,11,12])
 S.setLabel([3,4,5,7,8,9])
 V.setLabel([0,1,2,3,4,5])

 A2_inv=V*S*U
 A2_inv.permute([0,1,2,10,11,12],3)
 A2_inv.setLabel([10,-13,-400,-10,13,-300])

 l_up=A2_inv*Env_s
 l_up.permute([10,-13,-400],3)
 l_dp=copy.copy(l_up)
 l_dp.setLabel([-10,13,-300])

 return l_up, l_dp


#@profile
def  update_twotensor( mps_boundry_left, mps_boundry_right, PEPS_listten, N_x, N_y, U_ham0, U_ham, U_hamN, H0, H, HN , D, E_coulmn):

 bdi_mid=uni10.Bond(uni10.BD_IN,1)
 iden=uni10.UniTensor([bdi_mid])
 iden.identity()



 E_list_up=[None]*N_y
 for i in reversed(xrange(len(PEPS_listten))):
  if i==(len(PEPS_listten)-1):
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,2,3,4,5])
   Peps_list_conj.setLabel([6,7,3,9,10])

   mps_boundry_left[2*i+1].setLabel([11,1,15])
   mps_boundry_right[2*i+1].setLabel([13,4,16])

   mps_boundry_left[2*i].setLabel([12,6,11])
   mps_boundry_right[2*i].setLabel([14,9,13])


   iden.setLabel([5])
   Peps_list=Peps_list*iden
   iden.setLabel([10])
   Peps_list_conj=Peps_list_conj*iden

   iden.setLabel([15])
   mps_left=mps_boundry_left[2*i+1]*iden
   iden.setLabel([16])
   mps_right=mps_boundry_right[2*i+1]*iden

   result=(((((Peps_list*mps_left)*mps_right)*(Peps_list_conj))*mps_boundry_left[2*i])*mps_boundry_right[2*i])

   #result.permute([12,2,7,14],4)
   result.permute([12,7,2,14],4)
   E_list_up[i]=result
  else:
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,2,3,4,-5])
   Peps_list_conj.setLabel([6,7,3,9,-10])

   mps_boundry_left[2*i+1].setLabel([11,1,-15])
   mps_boundry_right[2*i+1].setLabel([13,4,-16])

   mps_boundry_left[2*i].setLabel([12,6,11])
   mps_boundry_right[2*i].setLabel([14,9,13])


   E_list_up[i+1].setLabel([-15,-5,-10,-16])
   result=((((((Peps_list*E_list_up[i+1])*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])*(Peps_list_conj))*mps_boundry_left[2*i])*mps_boundry_right[2*i])
   result.permute([12,2,7,14],4)
   E_list_up[i]=result




 E_list_down=[None]*N_y
 for i in xrange(len(PEPS_listten)):
  if i==0:
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,2,3,4,5])
   Peps_list_conj.setLabel([6,7,3,9,10])

   mps_boundry_left[2*i].setLabel([15 ,1,11])
   mps_boundry_right[2*i].setLabel([16,4,13])

   mps_boundry_left[2*i+1].setLabel([11,6,12])
   mps_boundry_right[2*i+1].setLabel([13,9,14])

   iden.setLabel([2])
   Peps_list=Peps_list*iden
   iden.setLabel([7])
   Peps_list_conj=Peps_list_conj*iden

   iden.setLabel([15])
   mps_left=mps_boundry_left[2*i]*iden
   iden.setLabel([16])
   mps_right=mps_boundry_right[2*i]*iden

   result=(((((Peps_list*mps_left)*mps_right)*(Peps_list_conj))*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])

   result.permute([12,10,5,14],4)
   E_list_down[i]=result
  else:
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,-2,3,4,5])
   Peps_list_conj.setLabel([6,-7,3,9,10])

   mps_boundry_left[2*i].setLabel([-15,1,11])
   mps_boundry_right[2*i].setLabel([-16,4,13])

   mps_boundry_left[2*i+1].setLabel([11,6,12])
   mps_boundry_right[2*i+1].setLabel([13,9,14])

   E_list_down[i-1].setLabel([-15,-7,-2,-16])

   result=((((((Peps_list*E_list_down[i-1])*mps_boundry_left[2*i])*mps_boundry_right[2*i])*(Peps_list_conj))*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])
   result.permute([12,10,5,14],4)
   E_list_down[i]=result


# for i in xrange(len(E_list_down)-1):
#   E_list_down[i].setLabel([12,5,10,14])
#   E_list_up[i+1].setLabel([12,5,10,14])
#   A=E_list_up[i+1]*E_list_down[i]
#   print i , A[0]








 for i_iter in xrange(1):

#  E_list_up=[None]*N_y
#  for i in reversed(xrange(len(PEPS_listten))):
#   if i==(len(PEPS_listten)-1):
#    Peps_list=copy.copy(PEPS_listten[i])
#    Peps_list_conj=copy.copy(PEPS_listten[i])

#    Peps_list.setLabel([1,2,3,4,5])
#    Peps_list_conj.setLabel([6,7,3,9,10])

#    mps_boundry_left[2*i+1].setLabel([11,1,15])
#    mps_boundry_right[2*i+1].setLabel([13,4,16])

#    mps_boundry_left[2*i].setLabel([12,6,11])
#    mps_boundry_right[2*i].setLabel([14,9,13])


#    iden.setLabel([5])
#    Peps_list=Peps_list*iden
#    iden.setLabel([10])
#    Peps_list_conj=Peps_list_conj*iden

#    iden.setLabel([15])
#    mps_left=mps_boundry_left[2*i+1]*iden
#    iden.setLabel([16])
#    mps_right=mps_boundry_right[2*i+1]*iden

#    result=(((((Peps_list*mps_left)*mps_right)*(Peps_list_conj))*mps_boundry_left[2*i])*mps_boundry_right[2*i])

#    #result.permute([12,2,7,14],4)
#    result.permute([12,7,2,14],4)
#    E_list_up[i]=result
#   else:
#    Peps_list=copy.copy(PEPS_listten[i])
#    Peps_list_conj=copy.copy(PEPS_listten[i])

#    Peps_list.setLabel([1,2,3,4,-5])
#    Peps_list_conj.setLabel([6,7,3,9,-10])

#    mps_boundry_left[2*i+1].setLabel([11,1,-15])
#    mps_boundry_right[2*i+1].setLabel([13,4,-16])

#    mps_boundry_left[2*i].setLabel([12,6,11])
#    mps_boundry_right[2*i].setLabel([14,9,13])


#    E_list_up[i+1].setLabel([-15,-5,-10,-16])
#    result=((((((Peps_list*E_list_up[i+1])*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])*(Peps_list_conj))*mps_boundry_left[2*i])*mps_boundry_right[2*i])
#    result.permute([12,2,7,14],4)
#    E_list_up[i]=result

  for i in xrange(N_y-1):
   Ham_u=1
   if i==0:
     Ham_u= U_ham0
   elif i==(N_y-2):
     Ham_u= U_hamN
   else:
    Ham_u= U_ham

   H_orig=1
   if i==0:
    H_orig= H0
   elif i==(N_y-2):
    H_orig= HN
   else:
    H_orig= H

  
   if i==N_y-2:
    PEPS_f, PEPS_s, E_val=Update_twotensor_local_last( PEPS_listten, E_list_down, E_list_up, mps_boundry_left, mps_boundry_right, i, Ham_u, H_orig,D)
    E_coulmn.append(E_val)
   else:
    PEPS_f, PEPS_s, E_val=Update_twotensor_local( PEPS_listten, E_list_down, E_list_up, mps_boundry_left, mps_boundry_right, i, Ham_u, H_orig,D)
    E_coulmn.append(E_val)

   PEPS_listten[i]=PEPS_f*1.0
   PEPS_listten[i+1]=PEPS_s*1.0

   if i==0:
    Peps_list=copy.copy(PEPS_listten[i])
    Peps_list_conj=copy.copy(PEPS_listten[i])
    Peps_list.setLabel([1,2,3,4,5])
    Peps_list_conj.setLabel([6,7,3,9,10])
    mps_boundry_left[2*i].setLabel([15 ,1,11])
    mps_boundry_right[2*i].setLabel([16,4,13])
    mps_boundry_left[2*i+1].setLabel([11,6,12])
    mps_boundry_right[2*i+1].setLabel([13,9,14])
    iden.setLabel([2])
    Peps_list=Peps_list*iden
    iden.setLabel([7])
    Peps_list_conj=Peps_list_conj*iden
    iden.setLabel([15])
    mps_left=mps_boundry_left[2*i]*iden
    iden.setLabel([16])
    mps_right=mps_boundry_right[2*i]*iden
    result=(((((Peps_list*mps_left)*mps_right)*(Peps_list_conj))*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])
    result.permute([12,10,5,14],4)
    E_list_down[i]=result
   else:
    Peps_list=copy.copy(PEPS_listten[i])
    Peps_list_conj=copy.copy(PEPS_listten[i])
    Peps_list.setLabel([1,-2,3,4,5])
    Peps_list_conj.setLabel([6,-7,3,9,10])
    mps_boundry_left[2*i].setLabel([-15,1,11])
    mps_boundry_right[2*i].setLabel([-16,4,13])
    mps_boundry_left[2*i+1].setLabel([11,6,12])
    mps_boundry_right[2*i+1].setLabel([13,9,14])
    E_list_down[i-1].setLabel([-15,-7,-2,-16])
    result=((((((Peps_list*E_list_down[i-1])*mps_boundry_left[2*i])*mps_boundry_right[2*i])*(Peps_list_conj))*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])
    result.permute([12,10,5,14],4)
    E_list_down[i]=result




def  Update_twotensor_local(PEPS_listten, E_list_down, E_list_up, mps_boundry_left, mps_boundry_right, Location, Ham, H_orig, D):

 bdi_mid=uni10.Bond(uni10.BD_IN,1)
 iden=uni10.UniTensor([bdi_mid])
 iden.identity()

 Peps_1=copy.copy(PEPS_listten[Location])
 Peps_2=copy.copy(PEPS_listten[Location+1])
 Peps_1s=copy.copy(PEPS_listten[Location])
 Peps_2s=copy.copy(PEPS_listten[Location+1])

 E_left=0
 if Location==0:
  iden.setLabel([16])
  Resul=copy.copy(iden)
  iden.setLabel([7])
  Resul=Resul*iden
  iden.setLabel([-7])
  Resul=Resul*iden
  iden.setLabel([17])
  Resul=Resul*iden
  E_left=Resul
 else:
  E_left= copy.copy(E_list_down[Location-1])
  E_left.setLabel([16,7,-7,17])

 E_right=0
 if Location>=(len(PEPS_listten)-2):
  iden.setLabel([25])
  Resul=copy.copy(iden)
  iden.setLabel([15])
  Resul=Resul*iden
  iden.setLabel([-15])
  Resul=Resul*iden
  iden.setLabel([24])
  Resul=Resul*iden
  E_right=Resul
 else:
  #print Location
  E_right=copy.copy(E_list_up[Location+2])
  E_right.setLabel([25,15,-15,24])

 A=copy.copy(Peps_1)
 A.setLabel([6,7,3,9,10])
 A.permute([6,7,9,3,10],3)
 q,s,V=svd_parity5(A)
 s.setLabel([0,1])
 V.setLabel([0,3,10])
 r_u=V*s
 r_u.permute([1,3,10],2)
 
 q.setLabel([6,7,9,-100])
 r_u.setLabel([-100,3,10])

 r_d=copy.copy(r_u)
 r_d.setLabel([-200,-3,-10])

 q_d=copy.copy(q)
 q_d.setLabel([-6,-7,-9,-200])

 A=copy.copy(Peps_2)
 A.setLabel([11,10,13,14,15])
 A.permute([10,13,11,14,15],2)
 U,s,qq=svd_parity6(A)
 s.setLabel([0,1])
 U.setLabel([10,13,0])
 l_u=U*s
 l_u.permute([10,13,1],2)

 qq.setLabel([-300,11,14,15])
 l_u.setLabel([10,13,-300])

 l_d=copy.copy(l_u)
 l_d.setLabel([-10,-13,-400])
 
 qq_d=copy.copy(qq)
 qq_d.setLabel([-400,-11,-14,-15])
######################################################################

##############simple_update###########################
# A=copy.copy(r_u)
# A.setLabel([-10,2,3])
# B=copy.copy(l_u)
# B.setLabel([3,6,-30])
# Ham.setLabel([-2,-6,2,6])

# Teta=(A*B)*Ham
# Teta.permute([-10,-2,-6,-30],2)
# U, V, S= setTruncation3(Teta, D)
# U.setLabel([-10,-2,-3])
# V.setLabel([-3,-6,-30])
# S=Sqrt(S)
# S.setLabel([3,-3])
# r_up=U*S
# r_up.permute([-10,-2,3],2)
# r_up.setLabel([-100,3,10])
# l_up=V*S
# l_up.permute([3,-6,-30],2)
# l_up.setLabel([10,13,-300])

# PEPS_1s=r_up*q
# PEPS_1s.permute([0,1,2,3,4],3)
# PEPS_2s=l_up*qq
# PEPS_2s.permute([ 3,5,6,7,8],3)

##########################################

######################################################################
 mps_boundry_left[Location*2].setLabel([16,-6,18])
 mps_boundry_left[Location*2+1].setLabel([18,6,21])
 mps_boundry_left[Location*2+2].setLabel([21,-11,22])
 mps_boundry_left[Location*2+3].setLabel([22,11,25])

 mps_boundry_right[Location*2].setLabel([17,-9,19])
 mps_boundry_right[Location*2+1].setLabel([19,9,20])
 mps_boundry_right[Location*2+2].setLabel([20,-14,23])
 mps_boundry_right[Location*2+3].setLabel([23,14,24])

######################################################

 A=E_left*mps_boundry_left[Location*2]
 A=A*q_d
 A=A*mps_boundry_right[Location*2]
 A=A*mps_boundry_left[Location*2+1] 
 A=A*q
 A=A*mps_boundry_right[Location*2+1]
 
 B=E_right*mps_boundry_left[Location*2+3]
 B=B*qq
 B=B*mps_boundry_right[Location*2+3]
 B=B*mps_boundry_left[Location*2+2]
 B=B*qq_d
 B=B*mps_boundry_right[Location*2+2]
 
 N_ten=A*B
 N_ten.permute([-200,-400,-100,-300],2)
 N_ten=N_Positiv(N_ten)
 #print N_ten.printDiagram()
######################################################

 l_up=copy.copy(l_u)
 r_up=copy.copy(r_u)
 l_dp=copy.copy(l_up)
 r_dp=copy.copy(r_up)
 l_dp.setLabel([-10,-13,-400])
 r_dp.setLabel([-200,-3,-10 ])
 
 Ham.setLabel([51,52,53,54])

 H1=copy.copy(Ham)
 H1.transpose()
 H1.setLabel([-20,-40,51,52])
 H=Ham*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([-2,-6,2,6])
 H.setLabel([-3,-13,3,13])

 Ham.setLabel([-3,-13,3,13])
 
 iden_h=copy.copy(Ham)
 iden_h.setLabel([-3,-13,3,13])
 iden_h.identity()

 iden_h.setLabel([-3,-13,3,13])
 #Norm_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 #valf=Norm_h
 #print  "Init_norm", Norm_h[0] 

 r_up_init=copy.copy(r_up)
 r_dp_init=copy.copy(r_dp)
 l_up_init=copy.copy(l_up)
 l_dp_init=copy.copy(l_dp)

 valf=norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)

 E_1=200
 E_2=0
 E_0=0
 count=1
 for i in xrange(14):
  count=count+1
  E_2=E_1*1.0
  val=norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)
  if i==0: E_0=val;
  E_1=val
  #print i, val, abs((E_1-E_2)/E_1)
  if E_1>E_2 or abs(E_1)<1.0e-10  or abs((E_1-E_2)/E_1)<+1.0e-10:
   #print "break"
   #print E_0, E_1, abs((E_1-E_2)/E_1)
   r_up=copy.copy(r_up_init)
   r_dp=copy.copy(r_dp_init)
   l_up=copy.copy(l_up_init)
   l_dp=copy.copy(l_dp_init)
   break;
  else:
   r_up_init=copy.copy(r_up)
   r_dp_init=copy.copy(r_dp)
   l_up_init=copy.copy(l_up)
   l_dp_init=copy.copy(l_dp)

  r_up, r_dp=optimum_0(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)
  l_up, l_dp=optimum_1(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)

 val=norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)

 #print "Tru_row",  abs(valf), abs(val), (abs(valf)-abs(val))/abs(valf), count

 if abs(valf)<1.0e-10 or abs(valf)>1.0e+10:
   print "warning_norm_in_optimization",  abs(valf), "count", count



 PEPS_1=r_up*q
 PEPS_1.permute([6,7,3,9,10],3)
 PEPS_2=l_up*qq
 PEPS_2.permute([11,10,13,14,15],3)
 
 H_orig.setLabel([-3,-13,3,13])
 iden_h.setLabel([-3,-13,3,13])
 Norm_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 h_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*H_orig

 PEPS_1, PEPS_2=equall_dis_V(PEPS_1, PEPS_2, D)

 PEPS_1=max_ten(PEPS_1)
 PEPS_2=max_ten(PEPS_2)
 
 Maxa=MaxAbs(PEPS_1)
 Maxb=MaxAbs(PEPS_2)
# print Maxa, Maxb
 
 #print "Fnit_norm", Norm_h[0], h_h[0]/Norm_h[0]

 return PEPS_1, PEPS_2, h_h[0]/Norm_h[0]



def  Update_twotensor_local_last(PEPS_listten,E_list_down,E_list_up,mps_boundry_left,mps_boundry_right, Location, Ham, H_orig,D):

 bdi_mid=uni10.Bond(uni10.BD_IN,1)
 iden=uni10.UniTensor([bdi_mid])
 iden.identity()

 Peps_1=copy.copy(PEPS_listten[Location])
 Peps_2=copy.copy(PEPS_listten[Location+1])
 Peps_1s=copy.copy(PEPS_listten[Location])
 Peps_2s=copy.copy(PEPS_listten[Location+1])

 E_left=0
 if Location==0:
  iden.setLabel([16])
  Resul=copy.copy(iden)
  iden.setLabel([7])
  Resul=Resul*iden
  iden.setLabel([-7])
  Resul=Resul*iden
  iden.setLabel([17])
  Resul=Resul*iden
  E_left=Resul
 else:
  E_left= copy.copy(E_list_down[Location-1])
  E_left.setLabel([16,7,-7,17])

 E_right=0
 if Location>=(len(PEPS_listten)-2):
  iden.setLabel([25])
  Resul=copy.copy(iden)
  iden.setLabel([15])
  Resul=Resul*iden
  iden.setLabel([-15])
  Resul=Resul*iden
  iden.setLabel([24])
  Resul=Resul*iden
  E_right=Resul
 else:
  #print Location
  E_right=copy.copy(E_list_up[Location+2])
  E_right.setLabel([25,15,-15,24])

 A=copy.copy(Peps_1)
 A.setLabel([6,7,3,9,10])
 A.permute([6,7,9,3,10],3)
 q,s,V=svd_parity5(A)
 s.setLabel([0,1])
 V.setLabel([0,3,10])
 r_u=V*s
 r_u.permute([1,3,10],2)
 
 q.setLabel([6,7,9,-100])
 r_u.setLabel([-100,3,10])

 r_d=copy.copy(r_u)
 r_d.setLabel([-200,-3,-10])

 q_d=copy.copy(q)
 q_d.setLabel([-6,-7,-9,-200])

 A=copy.copy(Peps_2)
 A.setLabel([-11,10,-13,-14,-15])
 A.permute([10,-13,-11,-14,-15],2)
 U,s,qq=svd_parity6(A)
 s.setLabel([0,1])
 U.setLabel([10,-13,0])
 l_u=U*s
 l_u.permute([10,-13,1],2)

 qq.setLabel([-400,-11,-14,-15])
 l_u.setLabel([10,-13,-400])

 l_d=copy.copy(l_u)
 l_d.setLabel([-10,13,-300])
 
 qq_d=copy.copy(qq)
 qq_d.setLabel([-300,11,14,15])
######################################################################

##############simple_update###########################
# A=copy.copy(r_u)
# A.setLabel([-10,2,3])
# B=copy.copy(l_u)
# B.setLabel([3,6,-30])
# Ham.setLabel([-2,-6,2,6])

# Teta=(A*B)*Ham
# Teta.permute([-10,-2,-6,-30],2)
# U, V, S= setTruncation3(Teta, D)
# U.setLabel([-10,-2,-3])
# V.setLabel([-3,-6,-30])
# S=Sqrt(S)
# S.setLabel([3,-3])
# r_up=U*S
# r_up.permute([-10,-2,3],2)
# r_up.setLabel([-100,3,10])
# l_up=V*S
# l_up.permute([3,-6,-30],2)
# l_up.setLabel([10,-13,-400])

# PEPS_1s=r_up*q
# PEPS_1s.permute([0,1,2,3,4],3)
# PEPS_2s=l_up*qq
# PEPS_2s.permute([ 3,5,6,7,8],3)

##########################################

######################################################################
 mps_boundry_left[Location*2].setLabel([16,-6,18])
 mps_boundry_left[Location*2+1].setLabel([18,6,21])
 mps_boundry_left[Location*2+2].setLabel([21,-11,22])
 mps_boundry_left[Location*2+3].setLabel([22,11,25])


 mps_boundry_right[Location*2].setLabel([17,-9,19])
 mps_boundry_right[Location*2+1].setLabel([19,9,20])
 mps_boundry_right[Location*2+2].setLabel([20,-14,23])
 mps_boundry_right[Location*2+3].setLabel([23,14,24])

######################################################

 A=E_left*mps_boundry_left[Location*2]
 A=A*q_d
 A=A*mps_boundry_right[Location*2]
 A=A*mps_boundry_left[Location*2+1] 
 A=A*q
 A=A*mps_boundry_right[Location*2+1]

 B=E_right*mps_boundry_left[Location*2+3]
 B=B*qq_d
 B=B*mps_boundry_right[Location*2+3]
 B=B*mps_boundry_left[Location*2+2]
 B=B*qq
 B=B*mps_boundry_right[Location*2+2]
 
 N_ten=A*B
 N_ten.permute([-200,-300,-100,-400],2)
 N_ten=N_Positiv(N_ten)
 N_ten.setLabel([-200,-300,-100,-400])

######################################################

 
 l_up=copy.copy(l_u)
 r_up=copy.copy(r_u)
 l_dp=copy.copy(l_up)
 r_dp=copy.copy(r_up)
 l_dp.setLabel([-10,13,-300])
 r_dp.setLabel([-200,-3,-10 ])
 
 Ham.setLabel([51,52,53,54])

 H1=copy.copy(Ham)
 H1.transpose()
 H1.setLabel([-20,-40,51,52])
 H=Ham*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([-2,-6,2,6])
 H.setLabel([-3,-13,3,13])

 Ham.setLabel([-3,-13,3,13])
 
 
 iden_h=copy.copy(Ham)
 iden_h.setLabel([-3,-13,3,13])
 iden_h.identity()


 iden_h.setLabel([-3,-13,3,13])
 Norm_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 #valf=Norm_h
 #print  "Init_norm", Norm_h[0] 

 r_up_init=copy.copy(r_up)
 r_dp_init=copy.copy(r_dp)
 l_up_init=copy.copy(l_up)
 l_dp_init=copy.copy(l_dp)

 valf=norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)

 E_1=200
 E_2=0
 E_0=0
 count=1
 for i in xrange(14):
  count=count+1
  E_2=E_1*1.0
  val=norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)
  if i==0: E_0=val;
  E_1=val
  #print i, val, abs((E_1-E_2)/E_1)
  if E_1>E_2 or abs(E_1)<1.0e-10  or abs((E_1-E_2)/E_1)<+1.0e-10:
   #print "break"
   #print E_0, E_1, abs((E_1-E_2)/E_1)
   r_up=copy.copy(r_up_init)
   r_dp=copy.copy(r_dp_init)
   l_up=copy.copy(l_up_init)
   l_dp=copy.copy(l_dp_init)
   break;
  else:
   r_up_init=copy.copy(r_up)
   r_dp_init=copy.copy(r_dp)
   l_up_init=copy.copy(l_up)
   l_dp_init=copy.copy(l_dp)

  r_up, r_dp=optimum_0(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)
  l_up, l_dp=optimum_11(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)

 #val=norm_f_val(N_ten, l_u, r_u, r_d, l_d, l_up, r_up,l_dp, r_dp, Ham,iden_h,H)

 #print "Tru_row",  abs(valf[0]), abs(val), (abs(valf[0])-abs(val))/abs(valf[0]), count
 if abs(valf)<1.0e-10 or abs(valf)>1.0e+10:
   print "warning_norm_in_optimization",  abs(valf), "count", count

 PEPS_1=r_up*q
 PEPS_1.permute([6,7,3,9,10],3)
 PEPS_2=l_up*qq
 PEPS_2.permute([-11,10,-13,-14,-15],3)
 
 H_orig.setLabel([-3,-13,3,13])
 iden_h.setLabel([-3,-13,3,13])
 Norm_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 h_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*H_orig

 #print "Fnit_norm", Norm_h[0], h_h[0]/Norm_h[0]

 PEPS_1, PEPS_2=equall_dis_V(PEPS_1, PEPS_2, D)

 PEPS_1=max_ten(PEPS_1)
 PEPS_2=max_ten(PEPS_2)

 Maxa=MaxAbs(PEPS_1)
 Maxb=MaxAbs(PEPS_2)
 #print Maxa, Maxb


 return PEPS_1, PEPS_2, h_h[0]/Norm_h[0]













#@profile
def  update_twotensor_energy( mps_boundry_left, mps_boundry_right, PEPS_listten, N_x, N_y, U_ham0, U_ham, U_hamN, H0, H, HN , D, E_coulmn):

 bdi_mid=uni10.Bond(uni10.BD_IN,1)
 iden=uni10.UniTensor([bdi_mid])
 iden.identity()


 E_list_up=[None]*N_y
 for i in reversed(xrange(len(PEPS_listten))):
  if i==(len(PEPS_listten)-1):
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,2,3,4,5])
   Peps_list_conj.setLabel([6,7,3,9,10])

   mps_boundry_left[2*i+1].setLabel([11,1,15])
   mps_boundry_right[2*i+1].setLabel([13,4,16])

   mps_boundry_left[2*i].setLabel([12,6,11])
   mps_boundry_right[2*i].setLabel([14,9,13])


   iden.setLabel([5])
   Peps_list=Peps_list*iden
   iden.setLabel([10])
   Peps_list_conj=Peps_list_conj*iden

   iden.setLabel([15])
   mps_left=mps_boundry_left[2*i+1]*iden
   iden.setLabel([16])
   mps_right=mps_boundry_right[2*i+1]*iden

   result=(((((Peps_list*mps_left)*mps_right)*(Peps_list_conj))*mps_boundry_left[2*i])*mps_boundry_right[2*i])

   #result.permute([12,2,7,14],4)
   result.permute([12,7,2,14],4)
   E_list_up[i]=result
  else:
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,2,3,4,-5])
   Peps_list_conj.setLabel([6,7,3,9,-10])

   mps_boundry_left[2*i+1].setLabel([11,1,-15])
   mps_boundry_right[2*i+1].setLabel([13,4,-16])

   mps_boundry_left[2*i].setLabel([12,6,11])
   mps_boundry_right[2*i].setLabel([14,9,13])


   E_list_up[i+1].setLabel([-15,-5,-10,-16])
   result=((((((Peps_list*E_list_up[i+1])*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])*(Peps_list_conj))*mps_boundry_left[2*i])*mps_boundry_right[2*i])
   result.permute([12,2,7,14],4)
   E_list_up[i]=result




 E_list_down=[None]*N_y
 for i in xrange(len(PEPS_listten)):
  if i==0:
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,2,3,4,5])
   Peps_list_conj.setLabel([6,7,3,9,10])

   mps_boundry_left[2*i].setLabel([15 ,1,11])
   mps_boundry_right[2*i].setLabel([16,4,13])

   mps_boundry_left[2*i+1].setLabel([11,6,12])
   mps_boundry_right[2*i+1].setLabel([13,9,14])

   iden.setLabel([2])
   Peps_list=Peps_list*iden
   iden.setLabel([7])
   Peps_list_conj=Peps_list_conj*iden

   iden.setLabel([15])
   mps_left=mps_boundry_left[2*i]*iden
   iden.setLabel([16])
   mps_right=mps_boundry_right[2*i]*iden

   result=(((((Peps_list*mps_left)*mps_right)*(Peps_list_conj))*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])

   result.permute([12,10,5,14],4)
   E_list_down[i]=result
  else:
   Peps_list=copy.copy(PEPS_listten[i])
   Peps_list_conj=copy.copy(PEPS_listten[i])

   Peps_list.setLabel([1,-2,3,4,5])
   Peps_list_conj.setLabel([6,-7,3,9,10])

   mps_boundry_left[2*i].setLabel([-15,1,11])
   mps_boundry_right[2*i].setLabel([-16,4,13])

   mps_boundry_left[2*i+1].setLabel([11,6,12])
   mps_boundry_right[2*i+1].setLabel([13,9,14])

   E_list_down[i-1].setLabel([-15,-7,-2,-16])

   result=((((((Peps_list*E_list_down[i-1])*mps_boundry_left[2*i])*mps_boundry_right[2*i])*(Peps_list_conj))*mps_boundry_left[2*i+1])*mps_boundry_right[2*i+1])
   result.permute([12,10,5,14],4)
   E_list_down[i]=result


 for i_iter in xrange(1):


  for i in xrange(N_y-1):
   Ham_u=1
   if i==0:
     Ham_u= U_ham0
   elif i==(N_y-2):
     Ham_u= U_hamN
   else:
    Ham_u= U_ham

   H_orig=1
   if i==0:
    H_orig= H0
   elif i==(N_y-2):
    H_orig= HN
   else:
    H_orig= H

   if i==N_y-2:
    E_val=Update_twotensor_local_last_energy( PEPS_listten, E_list_down, E_list_up, mps_boundry_left, mps_boundry_right, i, Ham_u, H_orig,D)
    E_coulmn.append(E_val)
   else:
    E_val=Update_twotensor_local_energy( PEPS_listten, E_list_down, E_list_up, mps_boundry_left, mps_boundry_right, i, Ham_u, H_orig,D)
    E_coulmn.append(E_val)




def  Update_twotensor_local_energy(PEPS_listten,E_list_down,E_list_up,mps_boundry_left,mps_boundry_right, Location, Ham, H_orig,D):

 bdi_mid=uni10.Bond(uni10.BD_IN,1)
 iden=uni10.UniTensor([bdi_mid])
 iden.identity()

 Peps_1=copy.copy(PEPS_listten[Location])
 Peps_2=copy.copy(PEPS_listten[Location+1])
 Peps_1s=copy.copy(PEPS_listten[Location])
 Peps_2s=copy.copy(PEPS_listten[Location+1])

 E_left=0
 if Location==0:
  iden.setLabel([16])
  Resul=copy.copy(iden)
  iden.setLabel([7])
  Resul=Resul*iden
  iden.setLabel([-7])
  Resul=Resul*iden
  iden.setLabel([17])
  Resul=Resul*iden
  E_left=Resul
 else:
  E_left= copy.copy(E_list_down[Location-1])
  E_left.setLabel([16,7,-7,17])

 E_right=0
 if Location>=(len(PEPS_listten)-2):
  iden.setLabel([25])
  Resul=copy.copy(iden)
  iden.setLabel([15])
  Resul=Resul*iden
  iden.setLabel([-15])
  Resul=Resul*iden
  iden.setLabel([24])
  Resul=Resul*iden
  E_right=Resul
 else:
  E_right=copy.copy(E_list_up[Location+2])
  E_right.setLabel([25,15,-15,24])

 A=copy.copy(Peps_1)
 A.setLabel([6,7,3,9,10])
 A.permute([6,7,9,3,10],3)
 q,s,V=svd_parity5(A)
 s.setLabel([0,1])
 V.setLabel([0,3,10])
 r_u=V*s
 r_u.permute([1,3,10],2)
 
 q.setLabel([6,7,9,-100])
 r_u.setLabel([-100,3,10])

 r_d=copy.copy(r_u)
 r_d.setLabel([-200,-3,-10])

 q_d=copy.copy(q)
 q_d.setLabel([-6,-7,-9,-200])

 A=copy.copy(Peps_2)
 A.setLabel([11,10,13,14,15])
 A.permute([10,13,11,14,15],2)
 U,s,qq=svd_parity6(A)
 s.setLabel([0,1])
 U.setLabel([10,13,0])
 l_u=U*s
 l_u.permute([10,13,1],2)

 qq.setLabel([-300,11,14,15])
 l_u.setLabel([10,13,-300])

 l_d=copy.copy(l_u)
 l_d.setLabel([-10,-13,-400])
 
 qq_d=copy.copy(qq)
 qq_d.setLabel([-400,-11,-14,-15])
######################################################################


######################################################################
 mps_boundry_left[Location*2].setLabel([16,-6,18])
 mps_boundry_left[Location*2+1].setLabel([18,6,21])
 mps_boundry_left[Location*2+2].setLabel([21,-11,22])
 mps_boundry_left[Location*2+3].setLabel([22,11,25])


 mps_boundry_right[Location*2].setLabel([17,-9,19])
 mps_boundry_right[Location*2+1].setLabel([19,9,20])
 mps_boundry_right[Location*2+2].setLabel([20,-14,23])
 mps_boundry_right[Location*2+3].setLabel([23,14,24])

######################################################

 A=E_left*mps_boundry_left[Location*2]
 A=A*q_d
 A=A*mps_boundry_right[Location*2]
 A=A*mps_boundry_left[Location*2+1] 
 A=A*q
 A=A*mps_boundry_right[Location*2+1]
 
  
 B=E_right*mps_boundry_left[Location*2+3]
 B=B*qq
 B=B*mps_boundry_right[Location*2+3]
 B=B*mps_boundry_left[Location*2+2]
 B=B*qq_d
 B=B*mps_boundry_right[Location*2+2]
 
 N_ten=A*B
 N_ten.permute([-200,-400,-100,-300],2)
 N_ten=N_Positiv(N_ten)
 #print N_ten.printDiagram()
######################################################

 l_up=copy.copy(l_u)
 r_up=copy.copy(r_u)
 l_dp=copy.copy(l_up)
 r_dp=copy.copy(r_up)
 l_dp.setLabel([-10,-13,-400])
 r_dp.setLabel([-200,-3,-10 ])
 
 Ham.setLabel([51,52,53,54])

 H1=copy.copy(Ham)
 H1.transpose()
 H1.setLabel([-20,-40,51,52])
 H=Ham*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([-2,-6,2,6])
 H.setLabel([-3,-13,3,13])

 Ham.setLabel([-3,-13,3,13])
 
 iden_h=copy.copy(Ham)
 iden_h.setLabel([-3,-13,3,13])
 iden_h.identity()

 iden_h.setLabel([-3,-13,3,13])
 
 H_orig.setLabel([-3,-13,3,13])
 iden_h.setLabel([-3,-13,3,13])

 Norm_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 h_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*H_orig

 #print "Fnit_norm", Norm_h[0], h_h[0]/Norm_h[0]

 return  h_h[0]/Norm_h[0]












def  Update_twotensor_local_last_energy(PEPS_listten,E_list_down,E_list_up,mps_boundry_left,mps_boundry_right, Location, Ham, H_orig,D):

 bdi_mid=uni10.Bond(uni10.BD_IN,1)
 iden=uni10.UniTensor([bdi_mid])
 iden.identity()

 Peps_1=copy.copy(PEPS_listten[Location])
 Peps_2=copy.copy(PEPS_listten[Location+1])
 Peps_1s=copy.copy(PEPS_listten[Location])
 Peps_2s=copy.copy(PEPS_listten[Location+1])

 E_left=0
 if Location==0:
  iden.setLabel([16])
  Resul=copy.copy(iden)
  iden.setLabel([7])
  Resul=Resul*iden
  iden.setLabel([-7])
  Resul=Resul*iden
  iden.setLabel([17])
  Resul=Resul*iden
  E_left=Resul
 else:
  E_left= copy.copy(E_list_down[Location-1])
  E_left.setLabel([16,7,-7,17])

 E_right=0
 if Location>=(len(PEPS_listten)-2):
  iden.setLabel([25])
  Resul=copy.copy(iden)
  iden.setLabel([15])
  Resul=Resul*iden
  iden.setLabel([-15])
  Resul=Resul*iden
  iden.setLabel([24])
  Resul=Resul*iden
  E_right=Resul
 else:
  #print Location
  E_right=copy.copy(E_list_up[Location+2])
  E_right.setLabel([25,15,-15,24])

 A=copy.copy(Peps_1)
 A.setLabel([6,7,3,9,10])
 A.permute([6,7,9,3,10],3)
 q,s,V=svd_parity5(A)
 s.setLabel([0,1])
 V.setLabel([0,3,10])
 r_u=V*s
 r_u.permute([1,3,10],2)
 
 q.setLabel([6,7,9,-100])
 r_u.setLabel([-100,3,10])

 r_d=copy.copy(r_u)
 r_d.setLabel([-200,-3,-10])

 q_d=copy.copy(q)
 q_d.setLabel([-6,-7,-9,-200])

 A=copy.copy(Peps_2)
 A.setLabel([-11,10,-13,-14,-15])
 A.permute([10,-13,-11,-14,-15],2)
 U,s,qq=svd_parity6(A)
 s.setLabel([0,1])
 U.setLabel([10,-13,0])
 l_u=U*s
 l_u.permute([10,-13,1],2)

 qq.setLabel([-400,-11,-14,-15])
 l_u.setLabel([10,-13,-400])

 l_d=copy.copy(l_u)
 l_d.setLabel([-10,13,-300])
 
 qq_d=copy.copy(qq)
 qq_d.setLabel([-300,11,14,15])
######################################################################

##############simple_update###########################
# A=copy.copy(r_u)
# A.setLabel([-10,2,3])
# B=copy.copy(l_u)
# B.setLabel([3,6,-30])
# Ham.setLabel([-2,-6,2,6])

# Teta=(A*B)*Ham
# Teta.permute([-10,-2,-6,-30],2)
# U, V, S= setTruncation3(Teta, D)
# U.setLabel([-10,-2,-3])
# V.setLabel([-3,-6,-30])
# S=Sqrt(S)
# S.setLabel([3,-3])
# r_up=U*S
# r_up.permute([-10,-2,3],2)
# r_up.setLabel([-100,3,10])
# l_up=V*S
# l_up.permute([3,-6,-30],2)
# l_up.setLabel([10,-13,-400])

# PEPS_1s=r_up*q
# PEPS_1s.permute([0,1,2,3,4],3)
# PEPS_2s=l_up*qq
# PEPS_2s.permute([ 3,5,6,7,8],3)

##########################################

######################################################################
 mps_boundry_left[Location*2].setLabel([16,-6,18])
 mps_boundry_left[Location*2+1].setLabel([18,6,21])
 mps_boundry_left[Location*2+2].setLabel([21,-11,22])
 mps_boundry_left[Location*2+3].setLabel([22,11,25])


 mps_boundry_right[Location*2].setLabel([17,-9,19])
 mps_boundry_right[Location*2+1].setLabel([19,9,20])
 mps_boundry_right[Location*2+2].setLabel([20,-14,23])
 mps_boundry_right[Location*2+3].setLabel([23,14,24])

######################################################

 A=E_left*mps_boundry_left[Location*2]
 A=A*q_d
 A=A*mps_boundry_right[Location*2]
 A=A*mps_boundry_left[Location*2+1] 
 A=A*q
 A=A*mps_boundry_right[Location*2+1]

 B=E_right*mps_boundry_left[Location*2+3]
 B=B*qq_d
 B=B*mps_boundry_right[Location*2+3]
 B=B*mps_boundry_left[Location*2+2]
 B=B*qq
 B=B*mps_boundry_right[Location*2+2]
 
 N_ten=A*B
 N_ten.permute([-200,-300,-100,-400],2)
 N_ten=N_Positiv(N_ten)
 N_ten.setLabel([-200,-300,-100,-400])

######################################################

 
 l_up=copy.copy(l_u)
 r_up=copy.copy(r_u)
 l_dp=copy.copy(l_up)
 r_dp=copy.copy(r_up)
 l_dp.setLabel([-10,13,-300])
 r_dp.setLabel([-200,-3,-10 ])
 
 Ham.setLabel([51,52,53,54])

 H1=copy.copy(Ham)
 H1.transpose()
 H1.setLabel([-20,-40,51,52])
 H=Ham*H1
 H.permute([-20,-40,53,54],2)
 H.setLabel([-2,-6,2,6])
 H.setLabel([-3,-13,3,13])

 Ham.setLabel([-3,-13,3,13])
 
 
 iden_h=copy.copy(Ham)
 iden_h.setLabel([-3,-13,3,13])
 iden_h.identity()


 iden_h.setLabel([-3,-13,3,13])
 
 H_orig.setLabel([-3,-13,3,13])
 iden_h.setLabel([-3,-13,3,13])
 Norm_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*iden_h
 h_h=(((r_up*r_dp)*N_ten)*(l_up*l_dp))*H_orig

 #print "Fnit_norm", Norm_h[0], h_h[0]/Norm_h[0]

 return h_h[0]/Norm_h[0]





































def   rotate_all(PEPS_listten, N_x, N_y):

 PEPS_listten_t=[None]*N_x
 for i in xrange(N_x):
  PEPS_listten_t[i]=[None]*N_y
  
  
 for i in xrange(N_x):
  for j in xrange(N_y):
   ten=copy.copy(PEPS_listten[i][N_x-1-j])
   ten.setLabel([0,1,2,3,4])
   ten.permute([1,0,2,4,3],3)
   PEPS_listten_t[N_x-1-j][i]=ten*1.0




 return PEPS_listten_t






def sqrt_general(N2):
  N_init=copy.copy(N2)
  blk_qnums = N2.blockQnum()
  for qnum in blk_qnums:
   M=N2.getBlock(qnum)
   eig=M.eigh()
   
   e=Sqrt_mat(eig[0])
   U_trans=copy.copy(eig[1])
   U_trans.transpose()
   M=U_trans*e*eig[1]
   N_init.putBlock(qnum,M)
  return N_init
def Sqrt_mat(e):
 d=int(e.row())
 
 for q in xrange(d):
   ##print e[q] 
   if e[q] > 0:  
    e[q]=((e[q])**(1.00/2.00))
   else:  
    e[q]=0.0 
 return e  
 
def N_Positiv(N):
 N.setLabel([-200,-400,-100,-300])
 N1=copy.copy(N)
 N1.transpose()
 N=(N+N1)*(1.00/2.00)
 N1=copy.copy(N)
 N1.setLabel([2,3,-100,-300])
 N.setLabel([-200,-400,2,3])

 N=N*N1
 N.permute([-200,-400,-100,-300],2)
 N_final=sqrt_general(N)
 N_final.setLabel([-200,-400,-100,-300])
 N_final.permute([-200,-400,-100,-300], 2)
 return N_final              










#@profile
def update_energy_eff(PEPS_listmps, U_eval, U_eval0, U_evalN, d, D, Location, N_x, chi_express):

 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)

 bdiphy=uni10.Bond(uni10.BD_IN, d)
 bdophy=uni10.Bond(uni10.BD_OUT, d)

 bdi1=uni10.Bond(uni10.BD_IN, 1)
 bdo1=uni10.Bond(uni10.BD_OUT, 1)
 N_y=PEPS_listmps.N
 B_list=[None]*N_y
 if Location == 0:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
 elif Location==N_x-1:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
 else:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     B_list[i]=A


 B_list_u=[None]*N_y
 for i in xrange(N_y):
  B_list[i].setLabel([1,3,2,0,4])
  B_list_u[i]=copy.copy(B_list[i])
  B_list_u[i].permute([1,3,2,0,4],6)
  B_list_u[i].combineBond([3,2])
  B_list_u[i].combineBond([3,0])
  B_list_u[i].permute([1,3,4],2)

 list_bond=[]
 for q in xrange(len(B_list_u)):
   list_bond.append(B_list_u[q].bond(2).dim())

 mps_b=MPSclass.MPS(B_list_u[1].bond(1).dim(),max(list_bond)*4,len(B_list_u))

 for i in xrange(len(B_list_u)):
   mps_b[i]=copy.copy(B_list_u[i])


 Norm_init=mps_b.norm()
 #print Norm_init

 for i in xrange(N_y-1):
  row=B_list_u[i].bond(0).dim()*B_list_u[i].bond(1).dim()
  colm=B_list_u[i].bond(2).dim()
  if (row<=colm):
   U,V,s=MPSclass.setTruncation1(B_list_u[i],row)
  else:
   U,V,s=MPSclass.setTruncation1(B_list_u[i],colm)
  B_list_u[i]=copy.copy(U)
  s.setLabel([1,2])
  V.setLabel([2,3])
  V=s*V
  V.permute([1,3],1)
  B_list_u[i+1].setLabel([3,2,4])
  B_list_u[i+1]=V*B_list_u[i+1]
  B_list_u[i+1].permute([1,2,4],2)
  #A_l_can[i+1]=copy.copy(A_l[i+1])

 test_norm=B_list_u[N_y-1]*B_list_u[N_y-1]
 test_norm1=B_list_u[N_y-2]*B_list_u[N_y-2]

 #print test_norm[0]#,test_norm1[0]

#U_eval, U_eval0, U_evalN

 for i in xrange(N_y-1):
  A=uni10.UniTensor([ B_list_u[N_y-1-i].bond(0), B_list[N_y-1-i].bond(1), B_list[N_y-1-i].bond(2), B_list[N_y-1-i].bond(3), B_list_u[N_y-1-i].bond(2)])
  A.putBlock(B_list_u[N_y-1-i].getBlock())
  A.setLabel([1,3,2,0,4])

  B=uni10.UniTensor([ B_list_u[N_y-2-i].bond(0), B_list[N_y-2-i].bond(1), B_list[N_y-2-i].bond(2), B_list[N_y-2-i].bond(3), B_list_u[N_y-2-i].bond(2)])
  B.putBlock(B_list_u[N_y-2-i].getBlock())
  B.setLabel([-11,-3,-2,-10,1])

  if i==0:
    H_eff=U_evalN
    H_eff.setLabel([20,30,-2,2])
  elif i==N_y-2:
    H_eff=U_eval0
    H_eff.setLabel([20,30,-2,2])
  else:
    H_eff=U_eval
    H_eff.setLabel([20,30,-2,2])

  B.setLabel([-11,-3,-2,-10,1])
  B.permute([-11,-3,-10,-2,1],3)
  q,s,V=svd_parity5(B)
  s.setLabel([0,10])
  V.setLabel([0,2,3])
  r_u=V*s
  r_u.permute([10,2,3],2)
  r_u.setLabel([10,-2,1])
  
  q.setLabel([-11,-3,-10,10])
  r_u.setLabel([10,-2,1])

  A.setLabel([1,3,2,0,4])
  A.permute([1,2,3,0,4],2)
  U,s,qq=svd_parity6(A)
  s.setLabel([0,20])
  U.setLabel([1,2,0])
  l_u=U*s
  l_u.permute([1,2,20],2)

  qq.setLabel([20,3,0,4])
  l_u.setLabel([1,2,20])

 ##############QR_update###########################
  A=copy.copy(r_u)
  A.setLabel([-10,2,3])
  B=copy.copy(l_u)
  B.setLabel([3,6,-30])
  H_eff.setLabel([-2,-6,2,6])

  Teta=(A*B)*H_eff
  Teta.permute([-10,-2,-6,-30],2)
  Teta_mat=Teta.getBlock()
  col1=Teta_mat.col()
  row1=Teta_mat.row()
  dim_mat=1
  if col1<row1:
   dim_mat=col1
  else:
   dim_mat=row1
  #print "Info", dim_mat, chi_express
  if chi_express<dim_mat:
   U, V, S= setTruncation3(Teta, chi_express)
  else:
   U, V, S= setTruncation3(Teta, dim_mat)
  U.setLabel([-10,-2,-3])
  V.setLabel([-3,-6,-30])
  S=Sqrt(S)
  S.setLabel([3,-3])
  r_up=U*S
  r_up.permute([-10,-2,3],2)
  r_up.setLabel([10,-2,1])
  l_up=V*S
  l_up.permute([3,-6,-30],2)
  l_up.setLabel([1,2,20])

  U=q*r_up
  U.permute([-11,-3,-2,-10,1],4)
  V=qq*l_up
  V.permute([1,3,2,0,4],4)

  U.setLabel([-11, -3, 20, -10, 1])
  U.permute([-11, -3, 20, -10, 1],4)
  U.combineBond([-3,20])
  U.combineBond([-3,-10])
  U.permute([-11, -3, 1],2)

  V.setLabel([1, 3, 30, 0, 4])
  V.combineBond([3,30])
  V.combineBond([3,0])
  V.permute([1, 3, 4],2)



#  Theta=(A*H_eff)*B
#  #print Theta.printDiagram()
#  Theta.permute([-11, -3, 20, -10, 3, 30, 0, 4],4)
#  U, V, S=setTruncation2(Theta, D)
#  U.setLabel([-11, -3, 20, -10, -1])
#  S.setLabel([-1,1])
#  U=U*S
#  V.setLabel([1, 3, 30, 0, 4])

##################
#  Theta1=A*B
#  Theta2=U*V
#  Theta2.permute([-11, -3, 20, -10, 3, 30, 0, 4],4)
#  Theta2.setLabel([-11, -3, -2, -10, 3, 2, 0, 4])
##################

#  U.permute([-11, -3, 20, -10, 1],4)
#  U.combineBond([-3,20])
#  U.combineBond([-3,-10])
#  U.permute([-11, -3, 1],2)

#  V.combineBond([3,30])
#  V.combineBond([3,0])
#  V.permute([1, 3, 4],2)

#################################
#  norm_test1=Theta1*Theta1
#  norm_test2=Theta2*Theta2
#  norm_testf=Theta1*Theta2
#  print "i", i, norm_test1[0], norm_test2[0], norm_testf[0]/((norm_test2[0]*norm_test1[0])**(0.5))
##################################################
  B_list_u[N_y-1-i]=copy.copy(V)
  B_list_u[N_y-2-i]=copy.copy(U)

  B_list_u[N_y-1-i].setLabel([1,2,3])
  B_list_u[N_y-1-i].permute([2,3,1],2)


  row=B_list_u[N_y-1-i].bond(0).dim()*B_list_u[N_y-1-i].bond(1).dim()
  colm=B_list_u[N_y-1-i].bond(2).dim()
 
  if (row<=colm):
   U,V,s=MPSclass.setTruncation1(B_list_u[N_y-1-i],row)
  else:
   U,V,s=MPSclass.setTruncation1(B_list_u[N_y-1-i],colm)


  U.setLabel([2,3,1])
  U.permute([1,2,3],2)
  B_list_u[N_y-1-i]=copy.copy(U)
  s.setLabel([3,4])
  V.setLabel([4,5])
  V_f=(s*V)
  V_f.permute([3,5],1)
  B_list_u[N_y-2-i].setLabel([-1,2,5])
  B_list_u[N_y-2-i]=V_f*B_list_u[N_y-2-i]
  B_list_u[N_y-2-i].permute([-1,2,3],2)


 list_bond=[]
 for  q  in  xrange(len(B_list_u)):
   list_bond.append(B_list_u[q].bond(2).dim())

 mps_b=MPSclass.MPS( B_list_u[1].bond(1).dim(), max(list_bond), len(B_list_u))

 for  i  in  xrange(len(B_list_u)):
   mps_b[i]=copy.copy(B_list_u[i])


 #mps_b=mps_b.normalize()

 return mps_b










def matSx():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=(1.0)*uni10.Matrix(dim, dim, [0.0, 1.0, 1.00, 0.0])
  return Mat 

def matSz():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=(1.0)*uni10.Matrix(dim, dim, [1.0, 0, 0, -1.0]);
  return Mat 

def matSy():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=(1.0)*uni10.Matrix(dim, dim, [0.0, -1.00, 1.00, 0.00]);
  return Mat 


def matIden():
    spin_t=0.5
    dimT = int(2*spin_t + 1)
    Mat=uni10.Matrix(dimT, dimT,[1,0,0,1])
    return Mat



def Heisenberg(h, d_phys, Model):

    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heisenberg")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
    if Model=="ITF":
     ham =h[0]*uni10.otimes(sz,sz)*(-1)+(-0.2500)*h[1]*(uni10.otimes(iden,sx)+uni10.otimes(sx,iden))
    if Model=="Heis":
     ham =(h[0]*uni10.otimes(sz,sz)+h[1]*uni10.otimes(sx,sx)+(-1.0*h[1])*uni10.otimes(sy,sy))*(0.25)

    #print ham
    H.putBlock(ham)
    return H



def Heisenberg0(h, d_phys, Model):

    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heisenberg")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
    if Model=="ITF":
     ham =h[0]*uni10.otimes(sz,sz)*(-1)+(-0.2500)*float(h[1])*(uni10.otimes(iden,sx)+2.00*uni10.otimes(sx,iden))
    if Model=="Heis":
     ham =(h[0]*uni10.otimes(sz,sz)+h[1]*uni10.otimes(sx,sx)+(-1.0*h[1])*uni10.otimes(sy,sy))*(0.25)

    #print ham
    H.putBlock(ham)
    return H



def HeisenbergN(h, d_phys, Model):

    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heisenberg")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
    if Model=="ITF":
     ham =h[0]*uni10.otimes(sz,sz)*(-1)+(-0.2500)*h[1]*(2.00*uni10.otimes(iden,sx)+uni10.otimes(sx,iden))
    if Model=="Heis":
     ham =(h[0]*uni10.otimes(sz,sz)+h[1]*uni10.otimes(sx,sx)+(-1.0*h[1])*uni10.otimes(sy,sy))*(0.25)

    #print ham
    H.putBlock(ham)
    return H




def Mag(d_phys):
    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "sz")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham =-0.2500*(uni10.otimes(iden,sz)+uni10.otimes(sz,iden))
    ham =-0.2500*(uni10.otimes(iden,sz)+uni10.otimes(sz,iden))
    H.putBlock(ham)
    return H



def Mag0(d_phys):
    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "sz")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham =-0.2500*(uni10.otimes(iden,sz)+2.00*uni10.otimes(sz,iden))
    ham =-0.2500*(uni10.otimes(iden,sz)+uni10.otimes(sz,iden))
    H.putBlock(ham)
    return H



def MagN(d_phys):
    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "sz")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham =-0.2500*(2.00*uni10.otimes(iden,sz)+uni10.otimes(sz,iden))
    ham =-0.2500*(uni10.otimes(iden,sz)+uni10.otimes(sz,iden))
    H.putBlock(ham)
    return H



def rotate(PEPS_list):

  PEPS_list_copy=[copy.copy(PEPS_list[i])   for i in xrange(len(PEPS_list))]

  for i in xrange(len(PEPS_list)):
    PEPS_list_copy[i].setLabel([0,1,2,3,4])
    PEPS_list_copy[i].permute([3,1,2,0,4],3)
    PEPS_list_copy[i].setLabel([0,1,2,3,4])
  return PEPS_list_copy


def make_Env_singleLayer(PEPS_listten, Location, mps_boundry, d, chi_boundry, N_x):

 Peps_ket=[ copy.copy(PEPS_listten[i]) for i in xrange(len(PEPS_listten))]
 Peps_bra=[ copy.copy(PEPS_listten[i]) for i in xrange(len(PEPS_listten))]

 DX=0
 D=0
 if Location!=(N_x-1):
  DX=Peps_ket[0].bond(3).dim()
  D=Peps_bra[0].bond(3).dim()
 else:
  DX=Peps_ket[0].bond(0).dim()
  D=Peps_bra[0].bond(0).dim()

 #print "Location, D, DX", Location, DX, D


 bdiX=uni10.Bond(uni10.BD_IN, DX)
 bdoX=uni10.Bond(uni10.BD_OUT, DX)

 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)

 bdiphy=uni10.Bond(uni10.BD_IN, d)
 bdophy=uni10.Bond(uni10.BD_OUT, d)

 bdiX=uni10.Bond(uni10.BD_IN, DX)
 bdoX=uni10.Bond(uni10.BD_OUT, DX)

 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)

 IdenX=uni10.UniTensor([bdiX, bdoX])
 IdenX.identity()
 IdenX.setLabel([4,5])

 Iden=uni10.UniTensor([bdi, bdo])
 Iden.identity()
 Iden.setLabel([7,8])

 IdenX1=uni10.UniTensor([bdiX, bdoX])
 IdenX1.identity()
 IdenX1.setLabel([3,6])

 #####################################Zero################################################################
 bdi_1 = uni10.Bond(uni10.BD_IN, 1)
 bdo_1 = uni10.Bond(uni10.BD_IN, 1)
 Tem0=uni10.UniTensor([bdi_1])
 Tem0.identity()
 Tem0.setLabel([0])
 #print Tem0

 Tem1=uni10.UniTensor([bdi_1])
 Tem1.identity()
 Tem1.setLabel([1])
 #print Tem1

 Tem11=uni10.UniTensor([bdi_1])
 Tem11.identity()
 Tem11.setLabel([11])
 #print Tem11


 Tem10=uni10.UniTensor([bdi_1])
 Tem10.identity()
 Tem10.setLabel([10])
 #print Tem10


 if Location==0:
  mps_list=[]

  Peps_ket[0].setLabel([0,1,2,3,4])
  Peps_bra[0].setLabel([10,11,2,-3,-4])

  results=((Peps_ket[0]*Tem0)*Tem1)*((Peps_bra[0])*Tem11)
  results.permute([10, 4, 3, -4, -3], 5)
  results.combineBond([4,3])
  results.combineBond([4,-4])
  results.permute([10,-3,4],2)
  mps_list.append(results)
  ######################################################3

  results=(IdenX1*Iden)*IdenX

  results.permute([4,3,7,5,8,6],6)
  results.combineBond([4,3])
  results.combineBond([4,7])

  results.combineBond([5,8])
  results.permute([4,6,5],2)
  mps_list.append(results)
  ##################################Middle##############
  for  q  in  xrange(1,len(Peps_ket)-1):
   #print q
   Peps_ket[q].setLabel([0,1,2,3,4])
   Peps_bra[q].setLabel([10,11,2,-3,-4])
   Tem0.identity()
   Tem0.setLabel([0])
   Tem10.identity()
   Tem10.setLabel([10])

   results=((Peps_ket[q]*Tem0))*((Peps_bra[q])*Tem10)
   results.permute([1,11,4,3,-4,-3],6)
   results.combineBond([4,3])
   results.combineBond([4,-4])
   results.combineBond([1,11])
   results.permute([1,-3,4],2)
   mps_list.append(results)
   ######################################################3
   Iden.setLabel([7,8])
   IdenX.setLabel([4,5])
   IdenX1.setLabel([3,6])

   results=(IdenX1*Iden)*IdenX

   results.permute([4,3,7,5,8,6],6)
   results.combineBond([4,3])
   results.combineBond([4,7])

   results.combineBond([5,8])
   results.permute([4,6,5],2)
   mps_list.append(results)
 
  ##################################LAssssssstTTT#####################################################
  q=len(Peps_bra)-1

  results=(IdenX1*Iden)*IdenX

  results.permute([4,3,7,5,8,6],6)
  results.combineBond([4,3])
  results.combineBond([4,7])

  results.combineBond([5,8])
  results.permute([5,6,4],2)
  mps_list.append(results)

  Tem4=uni10.UniTensor([bdi_1])
  Tem4.identity()
  Tem4.setLabel([4])
  Tem10.identity()
  Tem10.setLabel([10])
  Tem11.identity()
  Tem11.setLabel([11])

  Peps_ket[q].setLabel([0,1,2,3,4])
  Peps_bra[q].setLabel([10,-1,2,-3,11])
  #print Peps_ket[q].printDiagram()
  results=((Peps_ket[q])*Tem4)*(((Peps_bra[q])*Tem10)*Tem11)
  results.permute([1,3,-1,-3,0],4)
  results.combineBond([1,3])
  results.combineBond([1,-1])
  results.permute([1,-3,0],2)
  mps_list.append(results)

  list_bond=[]
  for q in xrange(len(mps_list)):
    list_bond.append(mps_list[q].bond(2).dim())

  mps_boundry=MPSclass.MPS(mps_list[1].bond(1).dim(),max(list_bond),len(mps_list))

  for i in xrange(len(mps_list)):
    mps_boundry[i]=copy.copy(mps_list[i])

  E_0=mps_boundry.norm()
  mps_bound=mps_boundry.D
  if mps_boundry.D>chi_boundry:
    mps_boundry=mps_boundry.appSVD(chi_boundry)
    #print "Trunc", mps_bound, chi_boundry, (E_0-mps_boundry.norm())/E_0
  return mps_boundry





######################## Add next Layer ###########

 if Location!=0:
  mps_list=[None]*mps_boundry.N

###############################First-Layer###############################
  mps_list[0]=mps_boundry[0]*1.0


  mps_boundry[1].setLabel([5,0,6])
  Peps_ket[0].setLabel([0,1,2,3,4])
  Tem1.setLabel([1])


  result=(mps_boundry[1]*(Tem1*Peps_ket[0]))
  result.permute([5,6,2,3,4],5)
  result.combineBond([6,4])
  result.combineBond([3,2])
  result.permute([5,3,6],2)
  mps_list[1]=result*1.0
  ###########################################
  mps_boundry[2].setLabel([5,0,6])

  Iden.setLabel([0,10])
  IdenX.setLabel([-5,-6])

  #print mps_boundry[2].printDiagram(), Iden.printDiagram(), IdenX.printDiagram()
  result=(mps_boundry[2]*(Iden*IdenX))
  result.permute([5,6,-5,-6,10],5)
  #results.combineBond([5])
  result.combineBond([5,-5])
  result.combineBond([6,-6])
  result.permute([5,10,6],2)
  mps_list[2]=result*1.0

  ###############################################################################

  for q in xrange(3, mps_boundry.N,2):

   ###########################################
   if q<(mps_boundry.N-3):
    #print q, (q-1)/2

    mps_boundry[q].setLabel([5,0,6])
    Peps_ket[(q-1)/2].setLabel([0,1,2,3,4])

    result=(mps_boundry[q]*(Peps_ket[(q-1)/2]))
    result.permute([5,1,6,2,3,4],6)
    result.combineBond([5,1])
    result.combineBond([6,4])
    result.combineBond([3,2])
    result.permute([5,3,6],2)
    mps_list[q]=result*1.0

  ################
    mps_boundry[q+1].setLabel([5,0,6])
    Iden.setLabel([0,10])
    IdenX.setLabel([-5,-6])

    result=(mps_boundry[q+1]*(Iden*IdenX))
    result.permute([5,6,-5,-6,10],5)
    result.combineBond([5,-5])
    result.combineBond([6,-6])
    result.permute([5,10,6],2)
    mps_list[q+1]=result*1.0
   elif q==(mps_boundry.N-3):
    #print "inside", q+1, (q-1)/2,(q/2)+1
    mps_boundry[q].setLabel([5,0,6])
    Peps_ket[(q-1)/2].setLabel([0,1,2,3,4])

    result=mps_boundry[q]*Peps_ket[(q-1)/2]
    result.permute([5,1,6,2,3,4],6)
    result.combineBond([5,1])
    result.combineBond([6,4])
    result.combineBond([3,2])
    result.permute([5,3,6],2)
    mps_list[q]=result*1.0


    mps_boundry[q+1].setLabel([5,0,6])
    Peps_ket[(q/2)+1].setLabel([0,1,2,3,4])

    result=mps_boundry[q+1]*Peps_ket[(q/2)+1]
    result.permute([5,1,6,2,3,4],6)
    result.combineBond([5,1])
    result.combineBond([6,4])
    result.combineBond([3,2])
    result.permute([5,3,6],2)
    mps_list[q+1]=result*1.0
   else:
    #print "last", q
    mps_list[q]=mps_boundry[q]*1.0



    #mps_A=mps_A.appSVD(chi_p)

  list_bond=[]
  for q in xrange(len(mps_list)):
    list_bond.append(mps_list[q].bond(2).dim())

  mps_boundry=MPSclass.MPS(mps_list[1].bond(1).dim(),max(list_bond),len(mps_list))

  for i in xrange(len(mps_list)):
    mps_boundry[i]=copy.copy(mps_list[i])

  E_0=mps_boundry.norm()
  mps_bound=mps_boundry.D
  if mps_boundry.D>chi_boundry:
    mps_boundry=mps_boundry.appSVD(chi_boundry)
    #print "Trunc", mps_bound, chi_boundry, (E_0-mps_boundry.norm())/E_0


  #print "test", mps_boundry_new.norm()

  ######################### Next Absorption ####################################

  #mps_boundry=mps_boundry_ne*1.0

  if Location == (N_x-1):
   DX=1
   bdiX=uni10.Bond(uni10.BD_IN, DX)
   bdoX=uni10.Bond(uni10.BD_OUT, DX)

  for  q  in  xrange(0, mps_boundry.N, 2):
   if q==0:
    mps_boundry[q].setLabel([10,3,2])
    Peps_bra[q].setLabel([3,11,1,4,-2])
    Tem1.setLabel([11])
    result=((mps_boundry[q]*Peps_bra[q])*Tem1)
    result.permute([10,4,2,-2,1],5)
    result.combineBond([2,-2])
    result.combineBond([2,1])
    result.permute([10,4,2],2)
    mps_list[q]=result*1.


    t1=uni10.UniTensor([mps_boundry[q+1].bond(0),bdiX,bdiphy,mps_boundry[q+1].bond(2)])
    t1.setLabel([10,3,1,2])
    #print mps_boundry[q+1].printDiagram()
    t1.putBlock(mps_boundry[q+1].getBlock())

     #Iden.setLabel([0,10])
    Iden.setLabel([-5,-6])

    result=(t1*Iden)
    result.permute([10,-5,1,3,2,-6],5)
    result.combineBond([10,-5])
    result.combineBond([10,1])
    result.combineBond([2,-6])
    result.permute([10,3,2],2)
    mps_list[q+1]=result*1.0
   elif q< mps_boundry.N-2:
    #print "midle", q
    mps_boundry[q].setLabel([10,3,2])
    Peps_bra[q/2].setLabel([3,11,1,4,-2])

    result=((mps_boundry[q]*Peps_bra[q/2]))
    result.permute([10,11,4,2,-2,1],5)
    result.combineBond([10,11])
    result.combineBond([2,-2])
    result.combineBond([2,1])
    result.permute([10,4,2],2)
    mps_list[q]=result*1.0

    t1=uni10.UniTensor([mps_boundry[q+1].bond(0),bdiX,bdiphy,mps_boundry[q+1].bond(2)])
    t1.setLabel([10,3,1,2])
    t1.putBlock(mps_boundry[q+1].getBlock())
    Iden.setLabel([-5,-6])
    result=(t1*Iden)
    result.permute([10,-5,1,3,2,-6],5)
    result.combineBond([10,-5])
    result.combineBond([10,1])
    result.combineBond([2,-6])
    result.permute([10,3,2],2)
    mps_list[q+1]=result*1.0
   elif q==mps_boundry.N-2:
    #print "last", q
    t1=uni10.UniTensor([mps_boundry[q].bond(0),bdiX,bdiphy,mps_boundry[q].bond(2)])
    t1.setLabel([10,3,1,2])
    t1.putBlock(mps_boundry[q].getBlock())

    Iden.setLabel([-5,-6])

    result=(t1*Iden)
    result.permute([10,-5,1,3,2,-6],5)
    result.combineBond([10,-5])
    result.combineBond([2,-6])
    result.combineBond([2,1])
    result.permute([10,3,2],2)
    mps_list[q]=result*1.0

    #t1=uni10.UniTensor([mps_boundry[q].bond(0),bdiphy,bdiX,mps_boundry[q].bond(2)])
    mps_boundry[q+1].setLabel([10,3,2])
    Peps_bra[q/2].setLabel([3,-2,1,4,11])
    Tem1.setLabel([11])
    #print "Hi",(q+1)/2#, Peps_bra[5],Peps_bra[6], Peps_bra[7] 
    result=(mps_boundry[q+1]*(Peps_bra[q/2]*Tem1))
    result.permute([10,-2,1,4,2],5)
    result.combineBond([10,-2])
    result.combineBond([10,1])
    #result.combineBond([2,1])
    result.permute([10,4,2],2)
    mps_list[q+1]=result*1.0

  list_bond=[]
  for q in xrange(len(mps_list)):
    list_bond.append(mps_list[q].bond(2).dim())

  mps_boundry=MPSclass.MPS(mps_list[1].bond(1).dim(),max(list_bond),len(mps_list))

  for i in xrange(len(mps_list)):
    mps_boundry[i]=copy.copy(mps_list[i])

  mps_bound=mps_boundry.D
  E_0=mps_boundry.norm()
  if mps_boundry.D>chi_boundry:
    mps_boundry=mps_boundry.appSVD(chi_boundry)
    #print "Trunc", mps_bound, chi_boundry, (E_0-mps_boundry.norm())/E_0

  return mps_boundry















def  Norm_based_on_LR(MPS_R_right, MPS_R_left, D):

 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)
 E_a=0
 A_l=[]
 for i in xrange(MPS_R_left.N):

  Ten_L=uni10.UniTensor([MPS_R_left[i].bond(0), bdi, bdi, MPS_R_left[i].bond(2)], "Ten_R")
  Ten_L.putBlock(MPS_R_left[i].getBlock())
  Ten_L.setLabel([-1,-2,0,-3])

  Ten_R=uni10.UniTensor([MPS_R_right[i].bond(0), bdi, bdi, MPS_R_right[i].bond(2)], "Ten_R")
  Ten_R.putBlock(MPS_R_right[i].getBlock())
  Ten_R.setLabel([1,2,0,3])

  A_ten=Ten_R*Ten_L
  A_ten.permute([1,-1,2,-2,3,-3],4)
  A_ten.combineBond([1,-1])
  A_ten.combineBond([2,-2])
  A_ten.combineBond([3,-3])
  A_ten.permute([1,2,3],2)
  
  if i == 0:
    #print A_l[i]
    A_ten.setLabel([-1,-2,1])
    A_l_dag=copy.copy(A_ten)
    A_l_dag.setLabel([-1,-2,2])
    E_a=A_l_dag*A_ten
    E_a.permute([1,2],1)
    E_a.setLabel([-3,-4])
  elif i == (MPS_R_left.N-1):
    A_ten.setLabel([-3,-2,1])
    A_l_dag=copy.copy(A_ten)
    A_l_dag.setLabel([-4,-2,1])
    #print A_l[i].printDiagram()
    E_a=A_l_dag*(E_a*A_ten)
  else:
    A_ten.setLabel([-3,-2,1])
    A_l_dag=copy.copy(A_ten)
    A_l_dag.setLabel([-4,-2,2])
    E_a=A_l_dag*(E_a*A_ten)
    E_a.permute([1,2],1)
    E_a.setLabel([-3,-4])

# list_bond=[]
# for q in xrange(MPS_R_left.N):
#   list_bond.append(A_list[q].bond(2).dim())
# 
# print "hi", max(list_bond)
# 
# mps_R=MPSclass.MPS(A_list[1].bond(1).dim(),max(list_bond),MPS_R_left.N)

# for i in xrange(MPS_R_left.N):
#   mps_R[i]=copy.copy(A_list[i])
    
 return   E_a[0]





def make_ENVMPS(MPO_Ten, N_y, N_x, chi):

 Env_left=[None]*N_x
 cont_list=[None]*N_y
 for j in xrange(N_y):
  A=copy.copy(MPO_Ten[0][j])
  A.setLabel([1,2,3,4])
  A.permute([2,3,1,4],2)
  A.combineBond([3,1])
  A.permute([2,3,4],2)
  cont_list[j]=copy.copy(A)

 list_bond=[]
 for q in xrange(N_y):
   list_bond.append(cont_list[q].bond(2).dim())
 mps_A=MPSclass.MPS( cont_list[0].bond(1).dim(), max(list_bond), N_y, )
 for i in xrange(N_y):
   mps_A[i]=copy.copy(cont_list[i])

 if mps_A.D>chi:
   mps_A=mps_A.appSVD(chi)

 Env_left[0]=mps_A

 for q in xrange(N_x-1):
  for j in xrange(N_y):
   A=copy.copy(Env_left[q][j])
   A.setLabel([2,3,4])
   B=copy.copy(MPO_Ten[q+1][j])
   B.setLabel([3,5,6,7])
   Result=A*B
   Result.permute([2,5,6,4,7],3)
   Result.combineBond([2,5])
   Result.combineBond([4,7])
   Result.permute([2,6,4],2)
   Result.setLabel([2,3,4])
   Result.permute([2,3,4],2)
   cont_list[j]=copy.copy(Result)

  list_bond=[]
  for i in xrange(N_y):
    list_bond.append(cont_list[i].bond(2).dim())

  mps_A=MPSclass.MPS( cont_list[0].bond(1).dim(), max(list_bond), N_y, )

  for i in xrange(N_y):
   mps_A[i]=copy.copy(cont_list[i])

  if mps_A.D>chi and q!=(N_x-2):
    #print q, "q"
    mps_A=mps_A.appSVD(chi)

  Env_left[q+1]=mps_A

###############################

 Env_right=[None]*N_x
 cont_list=[None]*N_y
 for j in xrange(N_y):
   A=copy.copy(MPO_Ten[N_x-1][j])
   A.setLabel([1,2,3,4])
   A.permute([2,1,3,4],2)
   A.combineBond([1,3])
   A.permute([2,1,4],2)
   cont_list[j]=copy.copy(A)

 list_bond=[]
 for i in xrange(N_y):
    list_bond.append(cont_list[i].bond(2).dim())

 mps_A=MPSclass.MPS( cont_list[0].bond(1).dim(), max(list_bond), N_y, )
 for i in xrange(N_y):
   mps_A[i]=copy.copy(cont_list[i])

 if mps_A.D>chi:
   mps_A=mps_A.appSVD(chi)


 Env_right[N_x-1]=mps_A


 for q in xrange(N_x-1):
  for j in xrange(N_y):
   A=copy.copy(Env_right[N_x-1-q][j])
   A.setLabel([2,3,4])
   B=copy.copy(MPO_Ten[N_x-2-q][j])
   B.setLabel([5,6,3,7])
   Result=A*B
   Result.permute([6,2,5,7,4],3)
   Result.combineBond([6,2])
   Result.combineBond([7,4])
   Result.permute([6,5,7],2)
   cont_list[j]=copy.copy(Result)
  #print  cont_list[0].bond(1).dim(), cont_list[0].bond(2).dim()
  list_bond=[]
  for i in xrange(N_y):
    list_bond.append(cont_list[i].bond(2).dim())

  mps_A=MPSclass.MPS( cont_list[0].bond(1).dim(), max(list_bond), N_y, )
  for i in xrange(N_y):
   mps_A[i]=copy.copy(cont_list[i])

  if mps_A.D>chi and q != (N_x-2) :
    mps_A=mps_A.appSVD(chi)

  Env_right[N_x-2-q]=mps_A


 return Env_left, Env_right





def  make_boundry_MPO(PEPS_listten, N_y, N_x):

 MPO=[None]*N_x
 for i in xrange(N_x):
   MPO[i]=[None]*N_y

 for i in xrange(N_x):
  for j in xrange(N_y):
   A=copy.copy(PEPS_listten[i][j])
   A.setLabel([1,2,3,4,5])
   A_t=copy.copy(A)
   A_t.setLabel([-1,-2,3,-4,-5])
   A=A*A_t
   A.permute([1,-1,2,-2,4,-4,5,-5],4)
   A.combineBond([1,-1])
   A.combineBond([2,-2])
   A.combineBond([4,-4])
   A.combineBond([5,-5])
   A.permute([1,2,4,5],2)
   MPO[i][j]=copy.copy(A)

 return   MPO


def mps_to_tensor_left(PEPS_listmps, N_x,N_y,D, d,q):

 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)

 bdiphy=uni10.Bond(uni10.BD_IN, d)
 bdophy=uni10.Bond(uni10.BD_OUT, d)

 bdi1=uni10.Bond(uni10.BD_IN, 1)
 bdo1=uni10.Bond(uni10.BD_OUT, 1)
 B_list=[None]*N_y

 if q == 0:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
 elif q==N_x-1:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
 else:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
 return   B_list






def   mps_to_tensor_right( PEPS_listmps, N_x, N_y, D, d, q):

 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)

 bdiphy=uni10.Bond(uni10.BD_IN, d)
 bdophy=uni10.Bond(uni10.BD_OUT, d)

 bdi1=uni10.Bond(uni10.BD_IN, 1)
 bdo1=uni10.Bond(uni10.BD_OUT, 1)
 B_list=[None]*N_y

 if q == 0:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi1,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
 elif q==N_x-1:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo1,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
 else:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_first")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_Last")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
   else:
     A=uni10.UniTensor([bdi,PEPS_listmps[i].bond(0),bdiphy,bdo,PEPS_listmps[i].bond(2)], "A_middle")
     A.setLabel([0,1,2,3,4])
     A.permute([1,3,2,0,4],4)
     A.putBlock(PEPS_listmps[i].getBlock())
     A.permute([0,1,2,3,4],3)
     B_list[i]=A
 return   B_list




def  tensor_to_mps_left(B_list, N_x,N_y):

 A_list=[None]*N_y
 for i in xrange(N_y):
  A=copy.copy(B_list[i])
  A.setLabel([0,1,2,3,4])
  A.permute([1,0,2,3,4],4)
  A.combineBond([0,2])
  A.combineBond([0,3])
  A.permute([1,0,4],2)
  A_list[i]=A

 mps_A=MPSclass.MPS( A_list[1].bond(1).dim(), A_list[1].bond(0).dim(), N_y)
 for i in xrange(N_y):
  mps_A[i]=copy.copy(A_list[i])
 return   mps_A



def  tensor_to_mps_right(B_list, N_x,N_y):

 A_list=[None]*N_y
 for i in xrange(N_y):
  A=copy.copy(B_list[i])
  A.setLabel([0,1,2,3,4])
  A.permute([1,3,2,0,4],4)
  A.combineBond([3,2])
  A.combineBond([3,0])
  A.permute([1,3,4],2)
  A_list[i]=A

 mps_A=MPSclass.MPS( A_list[1].bond(1).dim(), A_list[1].bond(0).dim(), N_y)
 for i in xrange(N_y):
  mps_A[i]=copy.copy(A_list[i])
 return   mps_A


def Init_PEPS( N_y, N_x, D, d, q):
 bdi=uni10.Bond(uni10.BD_IN, D)
 bdo=uni10.Bond(uni10.BD_OUT, D)

 bdiphy=uni10.Bond(uni10.BD_IN, d)
 bdophy=uni10.Bond(uni10.BD_OUT, d)

 bdi1=uni10.Bond(uni10.BD_IN, 1)
 bdo1=uni10.Bond(uni10.BD_OUT, 1)
 A_list=[None]*N_y
 B_list=[None]*N_y

 if q == 0:
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi1,bdi1,bdiphy,bdo,bdo], "A_first")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
     #print A_list[i].printDiagram()
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi1,bdi,bdiphy,bdo,bdo1], "A_Last")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   else:
     A=uni10.UniTensor([bdi1,bdi,bdiphy,bdo,bdo], "A_middle")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A

 elif q==N_x-1:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,bdi1,bdiphy,bdo1,bdo], "A_first")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
     #print A_list[i].printDiagram()
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,bdi,bdiphy,bdo1,bdo1], "A_Last")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   else:
     A=uni10.UniTensor([bdi,bdi,bdiphy,bdo1,bdo], "A_middle")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
 else:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=uni10.UniTensor([bdi,bdi1,bdiphy,bdo,bdo], "A_first")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
     #print A_list[i].printDiagram()
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,bdi,bdiphy,bdo,bdo1], "A_Last")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   else:
     A=uni10.UniTensor([bdi,bdi,bdiphy,bdo,bdo], "A_middle")
     A.randomize()
     #A.orthoRand()
     B_list[i]=copy.copy(A)
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A

 mps_A=MPSclass.MPS( A_list[1].bond(1).dim(), A_list[1].bond(0).dim(), N_y, 'ortho')
 for i in xrange(N_y):
     mps_A[i]=copy.copy(A_list[i])

 norm=0.5*mps_A.norm()
 for q in xrange(len(B_list)):
   B_list[q]=B_list[q]*(1/(norm**(0.5/N_y)))






 if q == 0:
  for i in xrange(N_y):
   if i == 0:
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   elif i ==(N_y-1):
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   else:
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A

 elif q==N_x-1:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   elif i ==(N_y-1):
     A=uni10.UniTensor([bdi,bdi,bdiphy,bdo1,bdo1], "A_Last")
     A.randomize()
     #A.orthoRand()
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   else:
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
 else:
  A_list=[None]*N_y
  for i in xrange(N_y):
   if i == 0:
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   elif i ==(N_y-1):
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A
   else:
     A=copy.copy(B_list[i])
     A.setLabel([0,1,2,3,4])
     A.permute([1,0,2,3,4],2)
     A.combineBond([0,2])
     A.combineBond([0,3])
     A.permute([1,0,4],2)
     A_list[i]=A

 mps_A=MPSclass.MPS( A_list[1].bond(1).dim(), A_list[1].bond(0).dim(), N_y, 'ortho')
 for i in xrange(N_y):
     mps_A[i]=copy.copy(A_list[i])

 return mps_A





def inverse(Landa2):
 invLanda2=uni10.UniTensor(Landa2.bond())
 blk_qnums=Landa2.blockQnum()
 for qnum in blk_qnums:
  D=int(Landa2.getBlock(qnum).row())
  D1=int(Landa2.getBlock(qnum).col())
  invL2 = uni10.Matrix(D, D1,True)
  invLt = uni10.Matrix(D, D1,True)
  invLt=Landa2.getBlock(qnum,True)
  #print invLt[0], invLt[1], invLt[2], invLt[3]
  for i in xrange(D):
      invL2[i] = 0 if ((invLt[i].real) < 1.0e-10) else (1.00 / (invLt[i].real))

  invLanda2.putBlock(qnum,invL2)
 return invLanda2


#########  prerequisite functions  #############
def   setTruncation1(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1),theta.bond(2),bdo_mid])
    GB.assign([bdi_mid,theta.bond(3),theta.bond(4),theta.bond(5)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
def   setTruncationMPS(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_mergemps(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0), theta.bond(1),bdo_mid])
    GB.assign([bdi_mid,  theta.bond(2)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA

def   sv_mergemps(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs











def   sv_merge1(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs



    
    
    
  
  
 
 

def svd_parityrl(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(3).Qlist())
    bd2=uni10.Bond(uni10.BD_IN,theta.bond(4).Qlist())
    bd3=uni10.Bond(uni10.BD_IN,theta.bond(5).Qlist())

    GA=uni10.UniTensor([theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4),theta.bond(5)])
    LA=uni10.UniTensor([bd1,bd2,bd3,theta.bond(3),theta.bond(4),theta.bond(5)])
    GB=uni10.UniTensor([bd1,bd2,bd3,theta.bond(3),theta.bond(4),theta.bond(5)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB

def svd_parity(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(1).Qlist())

    GA=uni10.UniTensor([theta.bond(0),theta.bond(1)])
    LA=uni10.UniTensor([bd1,theta.bond(1)])
    GB=uni10.UniTensor([bd1,theta.bond(1)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


def svd_parity1(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim())
    #print bdi1
    GA=uni10.UniTensor([theta.bond(0),theta.bond(1), bdo1])
    LA=uni10.UniTensor([bdi1,bdo1])
    GB=uni10.UniTensor([bdi1,theta.bond(2),theta.bond(3)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


def svd_parity2(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(3).dim()*theta.bond(4).dim()*theta.bond(5).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(3).dim()*theta.bond(4).dim()*theta.bond(5).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())

    if bdi.dim()<=bdi1.dim():
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(3),theta.bond(4), theta.bond(5)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(3),theta.bond(4), theta.bond(5)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB




def svd_parity5(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(3).dim()*theta.bond(4).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(3).dim()*theta.bond(4).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())

    if bdi.dim()<=bdi1.dim():
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


def svd_parity6(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim())

    if bdi.dim()<=bdi1.dim():
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(2),theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(2),theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB







def setTruncation2(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3), bdo_mid])
    GB.assign([bdi_mid, theta.bond(4), theta.bond(5), theta.bond(6), theta.bond(7)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA


def setTruncation3(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1), bdo_mid])
    GB.assign([bdi_mid, theta.bond(2), theta.bond(3)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA

def sv_merge1(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs


def svd_parity3(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(4).dim()*theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(4).dim()*theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim())

    if bdi.dim()<=bdi1.dim():
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(4),theta.bond(5), theta.bond(6),theta.bond(7)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(4),theta.bond(5), theta.bond(6),theta.bond(7)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB

def svd_parity4(theta):
    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim()*theta.bond(8).dim()*theta.bond(9).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim()*theta.bond(8).dim()*theta.bond(9).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())

    if bdi.dim()<=bdi1.dim():
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3),theta.bond(4), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(5),theta.bond(6), theta.bond(7),theta.bond(8),theta.bond(9)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3),theta.bond(4), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(5),theta.bond(6), theta.bond(7),theta.bond(8),theta.bond(9)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


# def svd_parity3(theta):
# 
#  bd1=uni10.Bond(uni10.BD_IN,theta.bond(3).Qlist())
#  bd2=uni10.Bond(uni10.BD_IN,theta.bond(4).Qlist())
#  bd3=uni10.Bond(uni10.BD_IN,theta.bond(5).Qlist())
#  #bd4=uni10.Bond(uni10.BD_IN,theta.bond(7).Qlist())
# 
#  GA=uni10.UniTensor([theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4),theta.bond(5)])
#  LA=uni10.UniTensor([bd1,bd2,bd3,theta.bond(4),theta.bond(5),theta.bond(6),theta.bond(7)])
#  GB=uni10.UniTensor([bd1,bd2,bd3,bd4,theta.bond(4),theta.bond(5),theta.bond(6),theta.bond(7)])
# 
#  svds = {}
#  blk_qnums = theta.blockQnum()
#  dim_svd=[]
#  for qnum in blk_qnums:
#      svds[qnum] = theta.getBlock(qnum).svd()
#      GA.putBlock(qnum, svds[qnum][0])
#      LA.putBlock(qnum, svds[qnum][1])
#      GB.putBlock(qnum, svds[qnum][2])
# 
# #    print LA
#  return GA, LA, GB

def Sqrt(Landa):
  Landa_cp=copy.copy(Landa)
  blk_qnums=Landa.blockQnum()
  for qnum in blk_qnums:
   D=int(Landa_cp.getBlock(qnum).col())
   Landa_cpm=Landa_cp.getBlock(qnum)
   Landam=Landa_cp.getBlock(qnum)
   for i in xrange(D):
    for j in xrange(D):
     if Landam[i*D+j] > 1.0e-12:
      Landa_cpm[i*D+j]=Landam[i*D+j]**(1.00/2.00)
     else:
      Landa_cpm[i*D+j]=0
   Landa_cp.putBlock(qnum,Landa_cpm)
  return Landa_cp 


def Sqrt_minor(Landa):
  Landa_cp=copy.copy(Landa)
  blk_qnums=Landa.blockQnum()
  for qnum in blk_qnums:
   D=int(Landa_cp.getBlock(qnum).col())
   Landa_cpm=Landa_cp.getBlock(qnum)
   Landam=Landa_cp.getBlock(qnum)
   for i in xrange(D):
    for j in xrange(D):
     if Landam[i*D+j] > 1.0e-10:
      Landa_cpm[i*D+j]=Landam[i*D+j]**(1.00/2.00)
     else:
      Landa_cpm[i*D+j]=0
   Landa_cp.putBlock(qnum,Landa_cpm)
  return Landa_cp 

