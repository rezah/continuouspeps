import pyUni10 as uni10
import copy
import numpy as np
from numpy import linalg as LA
import MPSclass 
import MPSFunctions as UD
import matplotlib.pyplot as plt
import matplotlib
import pylab

N_x=60
lat_sp=1.0/N_x
mass=0.500
mu_0=-9.0
V_0=-9.0
g_val=1.000
Per_cos=10                  #periodicity
D=10
d_in=2
d_out=8   #d_out<=d_in*d_in
accuracy=+1.0e-10
Model="Heis"               #ITF, Heis#
Start_tebd=0.02
N_tebd=20
N_iter_QR=20
N_iter_tebd=100
Grid__coupling=0.00
N_coupling_probe=1



#mps_A=MPSclass.MPS(d_out,D,N_x/2, "ortho")
##mps_A=mps_A.normalize()
#Q_list=UD.Init_Q_list(N_x/2, d_in, d_out)
#H_list=UD.make_H_list(N_x, g_val, d_in, lat_sp, mu_0, mass, V_0,Per_cos)
#HA_list=UD.Ascend_f(Q_list, H_list)


#UD.Store_mps(mps_A, N_x/2)
#UD.Reload_mps(mps_A, N_x/2)
#UD.Store_Q_list(Q_list, N_x/2)
#UD.Reload_Q_list(Q_list, N_x/2)

#UD.increase_physicalbond(Q_list,mps_A, N_x/2, d_out)
#UD.increase_HilbertSpace(Q_list, N_x/2, d_in)


mps_A=MPSclass.MPS(d_out,D,N_x/2, "ortho")
H1_list=UD.make_H_list(N_x*2, g_val, 2, lat_sp, mu_0, mass, V_0,Per_cos)
Q_list=UD.Init_Q_list(N_x, 2, 4)
H_list=UD.Ascend_f(Q_list, H1_list)
Q_list=UD.Init_Q_list(N_x/2, 4, d_out)
HA_list=UD.Ascend_f(Q_list, H_list)





#N_list=UD.make_N_list(N_x)



h_list=[]
E_iter_list=[]



#E_row=[]
E_0=1.0
E_1=1.0



for x_coupling in xrange(N_coupling_probe):

 if x_coupling==0:
  j_coupling=g_val
 else:
  j_coupling=g_val+Grid__coupling



 List_delN=UD.Short_TrotterSteps(N_iter_tebd)

 #print g_val, List_delN


 for i in xrange(N_iter_QR):

  E_row=[]
  UD.Energy_calMPS(mps_A, HA_list, N_x/2, E_row)
  E_0=sum(E_row)/(N_x)
  #print "E_Q", i, E_0


  UD.update_Q(mps_A, H_list, Q_list,N_x)
  #UD.update_Q(mps_A, H_list, Q_list,N_x)
  HA_list=UD.Ascend_f(Q_list, H_list)
  UD.Store_Q_list(Q_list, N_x/2)

  E_row=[]
  UD.Energy_calMPS(mps_A, HA_list, N_x/2, E_row)
  E_0=sum(E_row)/N_x
  #print "E_Q", i, E_0


  mps_A, Start_tebd=UD.TEBD(List_delN, Q_list, H_list, HA_list, N_x, mps_A, E_iter_list, D, accuracy,Start_tebd,N_tebd, i)

#  p_list=UD.Potential(N_x, mu_0, V_0, lat_sp, Per_cos)
#  n_list=UD.particle_density_f(mps_A, Q_list, d_in)
#  K_list=UD.kinetic_energy_f(mps_A, Q_list, d_in,g_val,mass)
#  I_list=UD.Interaction_energy_f(mps_A, Q_list, d_in,g_val,mass)


#  print   n_list

  count_list=[  i*lat_sp   for i in xrange(len(E_iter_list)) ]
#  count_list1=[  i*lat_sp   for i in xrange(len(p_list)) ]
#  count_list2=[  i*lat_sp   for i in xrange(len(I_list)) ]

  plt.plot( count_list, E_iter_list,'ro',label='ITF' ,markersize=10)
#  plt.plot( count_list1, p_list,'bx',label='Poterntial',markersize=10)
#  plt.plot( count_list2, K_list,'g^',label='Kinetic',markersize=10)
#  plt.plot( count_list2, I_list,'c>',label='Interaction',markersize=10)

  plt.xlabel('iteration', fontsize=20)
  plt.ylabel('E', fontsize=20)
#  plt.legend(loc='upper right')
  plt.savefig('Energy.pdf')
#  #plt.show()
  plt.close() 


  file = open("E_iter_list.txt", "w")
  for index in range(len(E_iter_list)):
   file.write(str(index) + " " + str(E_iter_list[index]/2.0)+" "+ "\n")
  file.close()

#  file = open("n_list.txt", "w")
#  for index in range(len(n_list)):
#    file.write(str(count_list[index]) + " " + str(n_list[index])+" "+ "\n")
#  file.close()


#  file = open("K_list.txt", "w")
#  for index in range(len(K_list)):
#    file.write(str(count_list2[index]) + " " + str(K_list[index])+" "+ "\n")


#  file = open("I_list.txt", "w")
#  for index in range(len(I_list)):
#    file.write(str(count_list2[index]) + " " + str(I_list[index])+" "+ "\n")





