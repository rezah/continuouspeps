import pyUni10 as uni10
import copy
import numpy as np
import scipy as sp
from numpy import linalg as npLA
from scipy import linalg as LA
import MPSclass 
import math

def Short_TrotterSteps(N_iterF):
 List_delN=[]

 #Delta_N=(0.20, N_iterF)
 #List_delN.append(Delta_N)

# Delta_N=(0.150, N_iterF)
# List_delN.append(Delta_N)

 #Delta_N=(0.10, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.095, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.090, N_iterF)
 #List_delN.append(Delta_N)


 #Delta_N=(0.08, N_iterF)
 #List_delN.append(Delta_N)


 #Delta_N=(0.07, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.06, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.05, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.04, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.03, N_iterF)
 #List_delN.append(Delta_N)

 #Delta_N=(0.02, N_iterF)
 #List_delN.append(Delta_N)

 Delta_N=(0.01, N_iterF)
 List_delN.append(Delta_N)

 Delta_N=(0.009, N_iterF)
 List_delN.append(Delta_N)

# Delta_N=(0.008, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.007, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.006, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.005, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.004, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.003, N_iterF)
# List_delN.append(Delta_N)

# Delta_N=(0.002, N_iterF)
# List_delN.append(Delta_N)

# for i in xrange(5, 1, -1):
#  Delta_N=(i*(1.0/10),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(10, 1, -1):
#  Delta_N=(i*(1.0/100),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(5, 5, -1):
#  Delta_N=(i*(1.0/100),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(10, 1, -1):
#  Delta_N=(i*(1.0/1000),N_iterF)
#  List_delN.append(Delta_N)

# for i in xrange(10, 0, -1):
#  Delta_N=(i*(1.0/10000),N_iterF)
#  List_delN.append(Delta_N)

 return List_delN




def   setTruncation1(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1),theta.bond(2),bdo_mid])
    GB.assign([bdi_mid,theta.bond(3),theta.bond(4),theta.bond(5)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )


def   sv_merge1(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs


def setTruncation3(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1), bdo_mid])
    GB.assign([bdi_mid, theta.bond(2), theta.bond(3)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA

##@profile
def  TEBD (List_delN, Q_list, H_list, HA_list, N_x, mps_A, E_iter_list, D, accuracy,Start_tebd,N_tebd,iter_n):

  E_min=1
  E_0=1
  E_1=100
  count_iter=0

  #for delta, N_iter in List_delN:
   #print "delta, N_iter", delta, N_iter
  
  
  print "Start_tebd", Start_tebd

  Start_tebd_f=10
  count=0

  for j_iter in xrange(1,20):
   #if iter_n==0: Start_tebd

   delta=Start_tebd/pow(2.0,j_iter)
   N_iter=50
   print delta, N_iter

   #H_list=make_H_list(N_x, g_val, d_in, lat_sp, mu_0, mass, V_0)
   HA_list=Ascend_f(Q_list, H_list)
   mpo_list, U_list=make_U_list(HA_list, N_x/2, delta)




   mps_Au=mps_A*1.0
   E_row=[]
   Energy_calMPS(mps_A, HA_list, N_x/2, E_row)
   E_0=sum(E_row)/(N_x)
   print "E_0_tebd", E_0#, E_row

   if abs(delta)< 1.0e-6:
    print "delta is small",  abs(delta)
    break 


   #Q_list=update_Q(mps_A, H_list, Q_list)
   for q_iter in xrange(N_iter):


 ########################### Row #############################
    #mps_A=update_mpsMPO(mps_A, mpo_list, N_x/2, D)
    #mps_A=update_mpsNormal(mps_A, U_list, N_x, D)
    mps_A=update_mpsSequantial(mps_A, U_list, N_x/2, D)
    
    
    
    
    
    
    if q_iter==0:
     E_row=[]
     Energy_calMPS(mps_A, HA_list, N_x/2, E_row)
     E_1=sum(E_row)/(N_x)
     E_iter_list.append(E_1)
     

#     if abs((E_1-E_0)/E_1)<accuracy:
#      print "loop_finished:reached_levelofaccuracy"
#      E_0=E_1*1.0
#      break


     if E_1 < E_0:
      Store_mps(mps_A, N_x/2)
      mps_Au=mps_A*1.0
      E_0=E_1*1.0
      if count==0: Start_tebd_f=delta*(2.5)
      #print "Start_tebd_f", Start_tebd_f
      count=count+1
     elif E_1>E_0:
      print "Didnot_get_lowered";
      mps_A=mps_Au*1.0
      break



  print "Start_tebd_f", Start_tebd_f
  Start_tebd=Start_tebd_f*1.0
  
  

#  E_row=[]
#  Energy_calMPS(mps_A, HA_list, N_x/2, E_row)
#  E_0=sum(E_row)/(N_x)
#  print "E_1_tebd", E_0#, E_row


  return mps_A, Start_tebd













##@profile
def val_CostQR( i, mps_A, H_list, Q_list, E_right, E_left):

 if i==0:
  H_list[2*i].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([4,3,5])
  mps_At=mps_A[i]*1
  mps_At.setLabel([4,-3,-5])
  E_right[i+1].setLabel([5,-5])
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  val_1=((((Q_t*H_list[2*i])*Q_list[i])*mps_A[i])*mps_At)*E_right[i+1]

 
  H_list[2*i+1].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([11,5,7])
  mps_At=mps_A[i]*1
  mps_At.setLabel([11,-5,-7])

  mps_A[i+1].setLabel([7,6,10])
  mps_AtP=mps_A[i+1]*1
  mps_AtP.setLabel([-7,-6,-10])


  E_right[i+2].setLabel([10,-10])

  Q_list[i].setLabel([3,1,5])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i+1].setLabel([2,4,6])
  Q_tP=Q_list[i+1]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_2=(((Q_list[i]*Q_t)*H_list[2*i+1])*(mps_A[i]*mps_At))*((Q_list[i+1]*Q_tP)*((mps_A[i+1]*mps_AtP)*E_right[i+2]))
  return val_1[0]+val_2[0]

 elif i==len(Q_list)-1:
  H_list[2*i].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([4,3,5])
  mps_At=mps_A[i]*1
  mps_At.setLabel([-4,-3,5])
  E_left[i-1].setLabel([4,-4])
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  val_1=((((Q_t*H_list[2*i])*Q_list[i])*mps_A[i])*mps_At)*E_left[i-1]

 
  H_list[2*i-1].setLabel([-1,-2,1,2])

  mps_A[i-1].setLabel([11,5,7])
  mps_At=mps_A[i-1]*1
  mps_At.setLabel([-11,-5,-7])

  mps_A[i].setLabel([7,6,10])
  mps_AtP=mps_A[i]*1
  mps_AtP.setLabel([-7,-6,10])


  E_left[i-2].setLabel([11,-11])

  Q_list[i-1].setLabel([3,1,5])
  Q_t=Q_list[i-1]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i].setLabel([2,4,6])
  Q_tP=Q_list[i]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_2=((((Q_list[i-1]*Q_t)*E_left[i-2])*H_list[2*i-1])*(mps_A[i-1]*mps_At))*((Q_list[i]*Q_tP)*((mps_A[i]*mps_AtP)))
  #print "val_2","end", val_2
  return val_1[0]+val_2[0]
 else:
  H_list[2*i].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([4,3,5])
  mps_At=mps_A[i]*1
  mps_At.setLabel([-4,-3,-5])
  E_left[i-1].setLabel([4,-4])
  E_right[i+1].setLabel([5,-5])
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  val_1=(((((Q_t*H_list[2*i])*Q_list[i])*mps_A[i])*E_left[i-1])*mps_At)*E_right[i+1]
  #print "inside", val_1
##################################################################################################   
  H_list[2*i-1].setLabel([-1,-2,1,2])

  mps_A[i-1].setLabel([11,5,7])
  mps_At=mps_A[i-1]*1
  mps_At.setLabel([-11,-5,-7])

  mps_A[i].setLabel([7,6,10])
  mps_AtP=mps_A[i]*1
  mps_AtP.setLabel([-7,-6,-10])

  E_left_t=1.0
  if i-2<0:
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_left_t=uni10.UniTensor([bdi, bdo])
   E_left_t.setLabel([1,2])
   E_left_t.identity()
   E_left_t.setLabel([11,-11])
  else:    
   E_left[i-2].setLabel([11,-11])
   E_left_t=1.0*E_left[i-2]

  E_right_t=1.0
  if (i+1)>(len(Q_list)-1):
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_right_t=uni10.UniTensor([bdi, bdo])
   E_right_t.setLabel([1,2])
   E_right_t.identity()
   E_right_t.setLabel([10,-10])
  else:
   E_right[i+1].setLabel([10,-10])
   E_right_t=1.0*E_right[i+1]

  Q_list[i-1].setLabel([3,1,5])
  Q_t=Q_list[i-1]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i].setLabel([2,4,6])
  Q_tP=Q_list[i]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_2=(((Q_list[i-1]*Q_t)*H_list[2*i-1])*((mps_A[i-1]*mps_At)*E_left_t))*((Q_list[i]*Q_tP)*((mps_A[i]*mps_AtP)*E_right_t))
  #print "val_2", val_2
##########################################################################################################
  H_list[2*i+1].setLabel([-1,-2,1,2])

  mps_A[i].setLabel([11,5,7])
  mps_At=mps_A[i]*1
  mps_At.setLabel([-11,-5,-7])

  mps_A[i+1].setLabel([7,6,10])
  mps_AtP=mps_A[i+1]*1
  mps_AtP.setLabel([-7,-6,-10])


  E_left_t=1.0
  if i-1<0:
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_left_t=uni10.UniTensor([bdi, bdo])
   E_left_t.setLabel([1,2])
   E_left_t.identity()
   E_left_t.setLabel([11,-11])
  else:    
   E_left[i-1].setLabel([11,-11])
   E_left_t=1.0*E_left[i-1]



  E_right_t=1.0
  if (i+2)>(len(Q_list)-1):
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_right_t=uni10.UniTensor([bdi, bdo])
   E_right_t.setLabel([1,2])
   E_right_t.identity()
   E_right_t.setLabel([10,-10])
  else:
   E_right[i+2].setLabel([10,-10])
   E_right_t=1.0*E_right[i+2]


  Q_list[i].setLabel([3,1,5])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i+1].setLabel([2,4,6])
  Q_tP=Q_list[i+1]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_3=(((Q_list[i]*Q_t)*H_list[2*i+1])*((mps_A[i]*mps_At)*E_left_t))*((Q_list[i+1]*Q_tP)*((mps_A[i+1]*mps_AtP)*E_right_t))
  #print "val_3", val_3
  return val_1[0]+val_2[0]+val_3[0]





#@profile
def Env_Q(i, mps_A, H_list, Q_list, E_right, E_left):

 if i==0:
  H_list[2*i].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([4,3,5])
  mps_At=mps_A[i]*1
  mps_At.setLabel([4,-3,-5])
  E_right[i+1].setLabel([5,-5])
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  val_1=((((H_list[2*i])*Q_t)*mps_A[i])*mps_At)*E_right[i+1]
  val_1.permute([1,2,3],2)
  val_1.setLabel([1,2,3])
  
  H_list[2*i+1].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([11,5,7])
  mps_At=mps_A[i]*1
  mps_At.setLabel([11,-5,-7])

  mps_A[i+1].setLabel([7,6,10])
  mps_AtP=mps_A[i+1]*1
  mps_AtP.setLabel([-7,-6,-10])


  E_right[i+2].setLabel([10,-10])

  Q_list[i].setLabel([3,1,5])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i+1].setLabel([2,4,6])
  Q_tP=Q_list[i+1]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_2=(((Q_t)*H_list[2*i+1])*(mps_A[i]*mps_At))*((Q_list[i+1]*Q_tP)*((mps_A[i+1]*mps_AtP)*E_right[i+2]))
  val_2.permute([3,1,5],2)
  val_2.setLabel([1,2,3])
  return val_1+val_2
 elif i==len(Q_list)-1:
  H_list[2*i].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([4,3,5])
  mps_At=mps_A[i]*1
  mps_At.setLabel([-4,-3,5])
  E_left[i-1].setLabel([4,-4])
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  val_1=((((Q_t*H_list[2*i]))*mps_A[i])*mps_At)*E_left[i-1]
  val_1.permute([1,2,3],2)
  val_1.setLabel([1,2,3])
 
  H_list[2*i-1].setLabel([-1,-2,1,2])

  mps_A[i-1].setLabel([11,5,7])
  mps_At=mps_A[i-1]*1
  mps_At.setLabel([-11,-5,-7])

  mps_A[i].setLabel([7,6,10])
  mps_AtP=mps_A[i]*1
  mps_AtP.setLabel([-7,-6,10])


  E_left[i-2].setLabel([11,-11])

  Q_list[i-1].setLabel([3,1,5])
  Q_t=Q_list[i-1]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i].setLabel([2,4,6])
  Q_tP=Q_list[i]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_2=((((Q_list[i-1]*Q_t)*E_left[i-2])*H_list[2*i-1])*(mps_A[i-1]*mps_At))*((Q_tP)*((mps_A[i]*mps_AtP)))
  val_2.permute([2,4,6],2)
  val_2.setLabel([1,2,3])
  return val_1+val_2
 else:
  H_list[2*i].setLabel([-1,-2,1,2])
  mps_A[i].setLabel([4,3,5])
  mps_At=mps_A[i]*1
  mps_At.setLabel([-4,-3,-5])
  E_left[i-1].setLabel([4,-4])
  E_right[i+1].setLabel([5,-5])
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  val_1=(((((Q_t*H_list[2*i]))*mps_A[i])*E_left[i-1])*mps_At)*E_right[i+1]
  val_1.permute([1,2,3],2)
  val_1.setLabel([1,2,3])

##################################################################################################   
  H_list[2*i-1].setLabel([-1,-2,1,2])

  mps_A[i-1].setLabel([11,5,7])
  mps_At=mps_A[i-1]*1
  mps_At.setLabel([-11,-5,-7])

  mps_A[i].setLabel([7,6,10])
  mps_AtP=mps_A[i]*1
  mps_AtP.setLabel([-7,-6,-10])

  E_left_t=1.0
  if i-2<0:
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_left_t=uni10.UniTensor([bdi, bdo])
   E_left_t.setLabel([1,2])
   E_left_t.identity()
   E_left_t.setLabel([11,-11])
  else:    
   E_left[i-2].setLabel([11,-11])
   E_left_t=1.0*E_left[i-2]

  E_right_t=1.0
  if (i+1)>(len(Q_list)-1):
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_right_t=uni10.UniTensor([bdi, bdo])
   E_right_t.setLabel([1,2])
   E_right_t.identity()
   E_right_t.setLabel([10,-10])
  else:
   E_right[i+1].setLabel([10,-10])
   E_right_t=1.0*E_right[i+1]

  Q_list[i-1].setLabel([3,1,5])
  Q_t=Q_list[i-1]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i].setLabel([2,4,6])
  Q_tP=Q_list[i]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_2=(((Q_list[i-1]*Q_t)*H_list[2*i-1])*((mps_A[i-1]*mps_At)*E_left_t))*((Q_tP)*((mps_A[i]*mps_AtP)*E_right_t))
  #print  val_2.printDiagram()
  val_2.permute([2,4,6],2)
  val_2.setLabel([1,2,3])

##########################################################################################################
  H_list[2*i+1].setLabel([-1,-2,1,2])

  mps_A[i].setLabel([11,5,7])
  mps_At=mps_A[i]*1
  mps_At.setLabel([-11,-5,-7])

  mps_A[i+1].setLabel([7,6,10])
  mps_AtP=mps_A[i+1]*1
  mps_AtP.setLabel([-7,-6,-10])


  E_left_t=1.0
  if i-1<0:
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_left_t=uni10.UniTensor([bdi, bdo])
   E_left_t.setLabel([1,2])
   E_left_t.identity()
   E_left_t.setLabel([11,-11])
  else:    
   E_left[i-1].setLabel([11,-11])
   E_left_t=1.0*E_left[i-1]



  E_right_t=1.0
  if (i+2)>(len(Q_list)-1):
   bdi = uni10.Bond(uni10.BD_IN, 1)
   bdo = uni10.Bond(uni10.BD_OUT, 1)
   E_right_t=uni10.UniTensor([bdi, bdo])
   E_right_t.setLabel([1,2])
   E_right_t.identity()
   E_right_t.setLabel([10,-10])
  else:
   E_right[i+2].setLabel([10,-10])
   E_right_t=1.0*E_right[i+2]


  Q_list[i].setLabel([3,1,5])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([3,-1,-5])

  Q_list[i+1].setLabel([2,4,6])
  Q_tP=Q_list[i+1]*1.0
  Q_tP.setLabel([-2,4,-6])

  val_3=(((Q_t)*H_list[2*i+1])*((mps_A[i]*mps_At)*E_left_t))*((Q_list[i+1]*Q_tP)*((mps_A[i+1]*mps_AtP)*E_right_t))
  val_3.permute([3,1,5],2)
  val_3.setLabel([1,2,3])

  return val_1+val_2+val_3


#@profile
def update_Q_local(Location, mps_A, H_list, Q_list, E_right, E_left):


  #val_QR=val_CostQR( Location, mps_A, H_list, Q_list, E_right, E_left)
  #print  "val_QR_0", val_QR


  Q_init=Q_list[Location]*1.0
  E2=10
  for i in xrange(20):
    E1=val_CostQR( Location, mps_A, H_list, Q_list, E_right, E_left)
    if abs(E1)< 1.0e-14:
      #print "E1< 1.0e-14"
      break
    #print i, E1, E2, abs((E2-E1)/E1)
    if abs(E1)>abs(E2) or i is 0:
     Q_init=Q_list[Location]*1.0
     if abs((E2-E1)/E1) < 1.0e-8:
      #print E2, E1, abs((E2-E1)/E1), i
      break
    else:
     print 'Notoptimized=i, E1, E2=', i,'  ', E1, '   ', E2
     Q_list[Location]=Q_init
     break
    Y=Env_Q(Location, mps_A, H_list, Q_list, E_right, E_left)
    #print Y.printDiagram() 
    svd=Y.getBlock().svd()
    temporary_matrix=svd[0]*svd[2]
    #temporary_update=temporary_matrix.transpose()
    temporary_update=1.0*temporary_matrix
    Q_list[Location].putBlock(temporary_update)
    Q_list[Location].setLabel([1,2,3])
    E2=copy.copy(E1)





#@profile
def   update_Q(mps_A, H_list, Q_list,N_x):

 E_left=[None]*mps_A.N
 for i in xrange(mps_A.N):
  if i == 0:
   mps_A[i].setLabel([-1,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,2])
   E_left[0]=mps_A_dag*mps_A[i]
   E_left[0].permute([1,2],1)
   E_left[0].setLabel([-3,-4])
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
  else:
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,2])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
   E_left[i].permute([1,2],1)
   E_left[i].setLabel([-3,-4])


 E_right=[None]*mps_A.N
 for i in reversed(xrange(mps_A.N)):
  if i == 0:
   mps_A[i].setLabel([-1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,-4])
   E_right[0]=mps_A_dag*mps_A[i]*E_right[1]
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_right[i]=mps_A_dag*(mps_A[i])
   E_right[i].setLabel([-3,-4])
  else:
   mps_A[i].setLabel([1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([2,-2,-4])
   E_right[i]=mps_A_dag*(E_right[i+1]*mps_A[i])
   E_right[i].permute([1,2],1)
   E_right[i].setLabel([-3,-4])


 #print H_list[0]

 HH_list=[   H_list[i]*1.0    for i in xrange(len(H_list)) ]
 e_max=[]
 for i in xrange(len(Q_list)):
########  making negetive spectrum  ##########
  h_ham=H_list[2*i].getBlock()
  e=h_ham.eigh()
  iden_m=h_ham*1.0
  iden_m.identity()
  N_col=e[0].col()
  list_energy1=[]
  for j in xrange(N_col):
   list_energy1.append(e[0][j])
  list_energy1=sorted(list_energy1, key=float)
  h_ham=h_ham+(-1.0)*iden_m*abs(max(list_energy1,key=abs))
  HH_list[2*i].putBlock(h_ham)
  e_max.append(max(list_energy1,key=abs))

  if 2*i+1>len(H_list)-2: break
  h_ham=H_list[2*i+1].getBlock()
  e=h_ham.eigh()
  iden_m=h_ham*1.0
  iden_m.identity()
  N_col=e[0].col()
  list_energy1=[]
  for j in xrange(N_col):
   list_energy1.append(e[0][j])
  list_energy1=sorted(list_energy1, key=float)
  h_ham=h_ham+(-1.0)*iden_m*abs(max(list_energy1,key=abs))
  HH_list[2*i+1].putBlock(h_ham)
  e_max.append(max(list_energy1,key=abs))



 #print H_list[0]


 for i in xrange(len(Q_list)):

  update_Q_local(i, mps_A, HH_list, Q_list, E_right, E_left)

#  E_row=[]
#  HA_list=Ascend_f(Q_list, H_list)
#  Energy_calMPS(mps_A, HA_list, N_x/2, E_row)
#  E_0=sum(E_row)/(N_x)
#  print "E_Q", i, E_0



# for i in xrange(len(Q_list)):
######### making negetive spectrum ##########
#  h_ham=H_list[2*i].getBlock()
#  iden_m=h_ham*1.0
#  iden_m.identity()
#  h_ham=h_ham+(1.0)*iden_m*abs(e_max[2*i])
#  H_list[2*i].putBlock(h_ham)

#  if 2*i+1>len(H_list)-2: break

#  h_ham=H_list[2*i+1].getBlock()
#  h_ham=h_ham+(1.0)*iden_m*abs(e_max[2*i+1])
#  H_list[2*i+1].putBlock(h_ham)







"""
Index ordering conventions:
iso_123:

  3
  |
(iso)
 / \
1   2
"""



def Init_Q_list(N_x, d_in, d_out):

  bdi = uni10.Bond(uni10.BD_IN, d_in)
  bdo = uni10.Bond(uni10.BD_OUT, d_out)

  T0=uni10.UniTensor([bdi, bdi, bdo])
  T0.setLabel([1,2,3])
  T0.identity()
  Q_list=[ T0*1.0 for i in xrange(N_x)   ]  

  return  Q_list

def Ascend_f(Q_list, H_list):

 HA_list=[None]*(len(Q_list)-1)
 for i in xrange(len(Q_list)):
  if i <len(Q_list)-1:
   #print "i", i
   Q_list[i].setLabel([1,2,3])
   Q_trans=Q_list[i]*1.0
   Q_trans.setLabel([-1,-2,-3])
   
   Q_list[i+1].setLabel([4,5,6])
   QP_trans=Q_list[i+1]*1.0
   QP_trans.setLabel([4,5,-6])

   H_list[2*i].setLabel([-1,-2,1,2])
   result=(Q_list[i]*H_list[2*i]*Q_trans)*(Q_list[i+1]*QP_trans)
   result.permute([-3,-6,3,6],2)
   HA_list[i]=result*1.0
################################################
   Q_list[i].setLabel([1,2,3])
   Q_trans=Q_list[i]*1.0
   Q_trans.setLabel([1,-2,-3])
   
   Q_list[i+1].setLabel([4,5,6])
   QP_trans=Q_list[i+1]*1.0
   QP_trans.setLabel([-4,5,-6])

   H_list[2*i+1].setLabel([-2,-4,2,4])
   result=(Q_list[i]*H_list[2*i+1]*Q_trans)*(Q_list[i+1]*QP_trans)
   result.permute([-3,-6,3,6],2)
   HA_list[i]=result*1.0+HA_list[i]

  elif i==len(Q_list)-1:
   #print "i", i
   Q_list[i-1].setLabel([1,2,3])
   Q_trans=Q_list[i-1]*1.0
   Q_trans.setLabel([1,2,-3])
   
   Q_list[i].setLabel([4,5,6])
   QP_trans=Q_list[i]*1.0
   QP_trans.setLabel([-4,-5,-6])

   H_list[2*i].setLabel([-4,-5,4,5])
   result=(Q_list[i-1]*Q_trans)*(Q_list[i]*H_list[2*i]*QP_trans)
   result.permute([-3,-6,3,6],2)
   HA_list[i-1]=result*1.0+HA_list[i-1]
 return HA_list




#@#@profile
def  update_mpsSequantial(mps_A, U_list, N_x,D):

 mps_B=mps_A*1.0

 for i in xrange(N_x-1):
  row=mps_B[i].bond(0).dim()*mps_B[i].bond(1).dim()
  colm=mps_B[i].bond(2).dim()
  if (row<=colm):
   U,V,s=MPSclass.setTruncation1(mps_B[i],row)
  else:
   U,V,s=MPSclass.setTruncation1(mps_B[i],colm)
  mps_B[i]=copy.copy(U)
  s.setLabel([1,2])
  V.setLabel([2,3])
  V=s*V
  V.permute([1,3],1)
  mps_B[i+1].setLabel([3,2,4])
  mps_B[i+1]=V*mps_B[i+1]
  mps_B[i+1].permute([1,2,4],2)
 
 
 #print U_list[N_x-2-i]
 
 for i in xrange(N_x-1):
  A=mps_B[N_x-1-i]*1.0
  A.setLabel([1,3,2])

  B=mps_B[N_x-2-i]*1.0
  B.setLabel([4,5,1])

  U_list[N_x-2-i].setLabel([-5,-3,5,3])
  Teta=(U_list[N_x-2-i])*(B*A)
  Teta.permute([4,-5,2,-3],2)
  Teta_mat=Teta.getBlock()
  col1=Teta_mat.col()
  row1=Teta_mat.row()
  dim_mat=1
  if col1<row1:
   dim_mat=col1
  else:
   dim_mat=row1
  #print "Info", dim_mat, chi_express
  if D<dim_mat:
   U, V, S= setTruncation3(Teta, D)
  else:
   U, V, S= setTruncation3(Teta, dim_mat)
  U.setLabel([4,-5,-30])
  V.setLabel([-30,2,-3])
  S=Sqrt(S)
  S.setLabel([30,-30])
  U=U*S
  U.permute([4,-5,30],2)
  U.setLabel([4,-5,1])
  V=V*S
  V.permute([30,2,-3],2)
  V.setLabel([1,2,-3])
  V.permute([1,-3,2],2)

  mps_B[N_x-1-i]=copy.copy(V)
  mps_B[N_x-2-i]=copy.copy(U)

  mps_B[N_x-1-i].setLabel([1,2,3])
  mps_B[N_x-1-i].permute([2,3,1],2)


  row=mps_B[N_x-1-i].bond(0).dim()*mps_B[N_x-1-i].bond(1).dim()
  colm=mps_B[N_x-1-i].bond(2).dim()
 
  if (row<=colm):
   U,V,s=MPSclass.setTruncation1(mps_B[N_x-1-i],row)
  else:
   U,V,s=MPSclass.setTruncation1(mps_B[N_x-1-i],colm)


  U.setLabel([2,3,1])
  U.permute([1,2,3],2)
  mps_B[N_x-1-i]=copy.copy(U)
  s.setLabel([3,4])
  V.setLabel([4,5])
  V_f=(s*V)
  V_f.permute([3,5],1)
  mps_B[N_x-2-i].setLabel([-1,2,5])
  mps_B[N_x-2-i]=V_f*mps_B[N_x-2-i]
  mps_B[N_x-2-i].permute([-1,2,3],2)

 #print mps_B.norm()
 mps_B=mps_B.normalize()
 return mps_B
















def  update_mpsMPO(mps_A, mpo_list, N_x,D):

 mps_B=mps_A*1.0
 for i in xrange(N_x):
  mps_A[i].setLabel([1,2,3])
  mpo_list[i].setLabel([-1,4,-3,2])
  #print mps_A[i].printDiagram(), mpo_list[i].printDiagram()
  mps_B[i]=mps_A[i]*mpo_list[i]
  mps_B[i].permute([1,-1,4,3,-3],3)
  mps_B[i].combineBond([1,-1])
  mps_B[i].combineBond([3,-3])
  mps_B[i].permute([1,4,3],2)

 mps_A=mps_B.appSVD(D)
 mps_A=mps_A.normalize()
 return mps_A

def  update_mpsNormal(mps_A, U_list, N_x,D):

 mps_B=mps_A*1.0
 for i in xrange(N_x-1):
  if i%2==0:

   U_list[i].setLabel([1,2,3,4])
   Uni_0=copy.copy(U_list[i])
   Uni_0.permute([1,3,2,4],2)
   U, s, V=svd_parity1(Uni_0)
   s=Sqrt(s)
   s.setLabel([-1,-2])
   V.setLabel([-2,2,4])
   V=V*s
   V.permute([-1,2,4],1)

   mps_B[i].setLabel([1,2,-3])
   U.setLabel([10,2,-3])
   s.setLabel([-3,3])
   U=U*s
   U.permute([10,2,3],2)


   mps_B[i]=mps_B[i]*U
   mps_B[i].permute([1,10,-3,3],4)
   mps_B[i].combineBond([-3,3])
   mps_B[i].permute([1,10,-3],2)

   V.setLabel([-3,10,4])
   mps_B[i+1].setLabel([3,4,5])

   mps_B[i+1]=mps_B[i+1]*V
   mps_B[i+1].permute([3,-3,10,5],4)
   mps_B[i+1].combineBond([3,-3])
   mps_B[i+1].permute([3,10,5],2)
   mps_B=mps_B.appSVD(D)

 for i in xrange(N_x-1):
  if i%2==1:
   U_list[i].setLabel([1,2,3,4])
   Uni_0=copy.copy(U_list[i])
   Uni_0.permute([1,3,2,4],2)
   U, s, V=svd_parity1(Uni_0)
   s.setLabel([-1,-2])
   s=Sqrt(s)
   V.setLabel([-2,2,4])
   V=V*s
   V.permute([-1,2,4],1)

   mps_B[i].setLabel([1,2,-3])
   U.setLabel([10,2,-3])
   s.setLabel([-3,3])
   U=U*s
   U.permute([10,2,3],2)


   mps_B[i]=mps_B[i]*U
   mps_B[i].permute([1,10,-3,3],4)
   mps_B[i].combineBond([-3,3])
   mps_B[i].permute([1,10,-3],2)

   V.setLabel([-3,10,4])
   mps_B[i+1].setLabel([3,4,5])

   mps_B[i+1]=mps_B[i+1]*V
   mps_B[i+1].permute([3,-3,10,5],4)
   mps_B[i+1].combineBond([3,-3])
   mps_B[i+1].permute([3,10,5],2)
   mps_B=mps_B.appSVD(D)


 mps_B=mps_B.normalize()
 return mps_B


##@profile
def  Energy_calMPS(mps_A, H_list, N_x, E_row):


 mps_A.normalize()
 E_left=[None]*mps_A.N
 for i in xrange(mps_A.N):
  if i == 0:
   mps_A[i].setLabel([-1,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,2])
   E_left[0]=mps_A_dag*mps_A[i]
   E_left[0].permute([1,2],1)
   E_left[0].setLabel([-3,-4])
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
  else:
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,2])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
   E_left[i].permute([1,2],1)
   E_left[i].setLabel([-3,-4])


 E_right=[None]*mps_A.N
 for i in reversed(xrange(mps_A.N)):
  if i == 0:
   mps_A[i].setLabel([-1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,-4])
   E_right[0]=mps_A_dag*mps_A[i]*E_right[1]
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_right[i]=mps_A_dag*(mps_A[i])
   E_right[i].setLabel([-3,-4])
  else:
   mps_A[i].setLabel([1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([2,-2,-4])
   E_right[i]=mps_A_dag*(E_right[i+1]*mps_A[i])
   E_right[i].permute([1,2],1)
   E_right[i].setLabel([-3,-4])

 
 for i in xrange(len(H_list)):
  

  H_list[i].setLabel([-3,-6,3,6])


  if i==0:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,-4])

    E_right[i+2].setLabel([4,-4])
    val=((H_list[i]*mps_A_dag)*mps_A[i])*(mps_A[i+1]*mps_A_dagN)*E_right[i+2]
    E_row.append(val[0])
    #print val
  elif i==len(H_list)-1:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,4])

    E_left[i-1].setLabel([1,-1])
    val=(((H_list[i]*mps_A_dag)*E_left[i-1])*mps_A[i])*(mps_A[i+1]*mps_A_dagN)
    E_row.append(val[0])
  else:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,-4])
    E_left[i-1].setLabel([1,-1])
    E_right[i+2].setLabel([4,-4])

    val=(((H_list[i]*mps_A_dag)*E_left[i-1])*mps_A[i])*(mps_A[i+1]*mps_A_dagN)*E_right[i+2]
    E_row.append(val[0])




# mps_A=mps_A.normalize()
# #mps_B=mps_A*1.0

# for i in xrange(len(H_list)):
#  #print "Hi", i
#  mps_B=mps_A*1.0
#  H_list[i].setLabel([1,2,3,4])
#  Uni_0=copy.copy(H_list[i])
#  Uni_0.permute([1,3,2,4],2)
#  U, s, V=svd_parity1(Uni_0)
#  U.setLabel([1,3,-1])
#  V.setLabel([-2,2,4])
#  s=Sqrt(s)
#  s.setLabel([-1,-2])
#  V=V*s
#  V.permute([-1,2,4],1)
#  U=U*s
#  U.permute([1,3,-2],2)
#  U.setLabel([1,3,-1])

#  mps_B[i].setLabel([-10,3,6])
#  mps_B[i+1].setLabel([6,4,7])
#  mps_B[i]=(mps_B[i]*U)
#  mps_B[i].permute([-10,1,6,-1],2)
#  mps_B[i].combineBond([6,-1])
#  mps_B[i].permute([-10,1,6],2)
#  
#  mps_B[i+1]=(mps_B[i+1]*V)
#  mps_B[i+1].permute([6,-1,2,7],3)
#  mps_B[i+1].combineBond([6,-1])
#  mps_B[i+1].permute([6,2,7],2)
#  E_val=mps_B.product(mps_A)
#  #Norm_val=mps_A.product(mps_A)
#  E_row.append(E_val)
#  #print mps_A.norm()
#  #print E_val





def  make_MPS_Q(Uni_list):

 U_even=[]
 V_even=[]

 U_odd=[]
 V_odd=[]
 
 for i in xrange(((len(Uni_list)-1)/2)+1):
  #print "StepsEven", i, 2*i
  Uni_list[2*i].setLabel([1,2,3,4])
  Uni_0=copy.copy(Uni_list[2*i])
  Uni_0.permute([1,3,2,4],2)
  U, s, V=svd_parity1(Uni_0)
  U.setLabel([1,5,-1])
  s.setLabel([-1,-2])
  V.setLabel([-2,2,6])
  V=V*s
  V.permute([-1,2,6],1)
  
  U_even.append(U)
  U_even.append(V)
  
 for i in xrange((len(Uni_list)-1)/2):
  Uni_list[2*i+1].setLabel([1,2,3,4])
  Uni_0=copy.copy(Uni_list[2*i+1])
  Uni_0.permute([1,3,2,4],2)
  U, s, V=svd_parity1(Uni_0)
  U.setLabel([1,3,-1])
  s.setLabel([-1,-2])
  V.setLabel([-2,2,4])
  V=V*s
  V.permute([-1,2,4],1)
  U_odd.append(U)
  U_odd.append(V)


 A_list=[None]*(len(Uni_list)+1)
 bdi=uni10.Bond(uni10.BD_IN, 1)
 A_temp=uni10.UniTensor([bdi])
 A_temp.identity()
 A_temp.setLabel([-3])
 for i in xrange(len(Uni_list)+1):
   if i==0:
    U_even[i].setLabel([1,5,-1])
    A=U_even[i]*A_temp
    A.permute([-3,1,5,-1],3)
    A.permute([-3,1,-1,5],2)
    A_list[i]=copy.copy(A)
   elif i == (len(Uni_list)):
    A=U_even[i]*A_temp
    A.permute([-1,2,6,-3],3)
    A.permute([-1,2,-3,6],2)
    A_list[i]=copy.copy(A)
   else:
    if (i%2) != 0:
     U_even[i].setLabel([-1,2,3])
     U_odd[i-1].setLabel([4,2,5])
     A=U_even[i]*U_odd[i-1]
     A.permute([-1,4,5,3],2)
     A_list[i]=copy.copy(A)
    else:
     U_even[i].setLabel([1,3,2])
     U_odd[i-1].setLabel([4,5,1])
     A=U_even[i]*U_odd[i-1]
     A.permute([4,5,2,3],2)
     A_list[i]=copy.copy(A)

 return   A_list

##@profile
def  make_U_list(H_list, N_x, delta):

 U_ham_list=[None]*(N_x-1)
 for i in xrange(N_x-1):
  U_ham = uni10.UniTensor(H_list[i].bond(), "U_ham")
  U_ham.putBlock(uni10.takeExp(-delta, H_list[i].getBlock()))
  U_ham_list[i]=U_ham*1.0


 #mps_Q=make_MPS_Q(U_ham_list)
 mps_Q=None
 return mps_Q, U_ham_list



def C_i(dim):
  Mat=uni10.Matrix(dim, dim)
  Mat.set_zero()
  for i in xrange(dim):
   for j in xrange(dim):
     if j==i+1:
      Mat[i*dim+j]=j**(0.5) 
  iden_m=Mat*1.0
  iden_m.identity()
  Mat2=Mat*1.0
  Mat2.transpose()
  #commutator=Mat2*Mat+(-1.0)*Mat*Mat2
  #print Mat, Mat2, Mat1 ,commutator
  return Mat , Mat2, iden_m


def increase_physicalbond( Q_list, mps_A, N_x, d_out):

 bdi = uni10.Bond( uni10.BD_IN, Q_list[0].bond(2).dim())
 bdo = uni10.Bond( uni10.BD_OUT, d_out)
 Increase_tensor = uni10.UniTensor( [bdi, bdo], "Increase_tensor")
 
 
 Increase_tensor.setLabel([-1, -2])
 Increase_tensor.identity()
 for i in xrange(N_x):
  Q_list[i].setLabel([0,1,-1])
  Q_list[i]=Q_list[i]*Increase_tensor
  Q_list[i].permute([0,1,-2],2)

  mps_A[i].setLabel([1,-1,2])
  mps_A[i]=mps_A[i]*Increase_tensor
  mps_A[i].permute([1,-2,2],2)



def increase_HilbertSpace( Q_list, N_x, d_out):

 bdi = uni10.Bond( uni10.BD_IN, Q_list[0].bond(0).dim())
 bdo = uni10.Bond( uni10.BD_OUT, d_out)
 Increase_tensor = uni10.UniTensor( [bdi, bdo], "Increase_tensor")

 bdi = uni10.Bond( uni10.BD_IN, Q_list[0].bond(1).dim())
 bdo = uni10.Bond( uni10.BD_OUT, d_out)
 Increase_tensor1 = uni10.UniTensor( [bdi, bdo], "Increase_tensor1")

 Increase_tensor.setLabel([10, -10])
 Increase_tensor.identity()
 Increase_tensor1.setLabel([20, -20])
 Increase_tensor1.identity()


 for i in xrange(N_x):
  Q_list[i].setLabel([10,20,-1])
  Q_list[i]=Q_list[i]*Increase_tensor
  Q_list[i]=Q_list[i]*Increase_tensor1
  Q_list[i].permute([-10,-20,-1],2)





def Potential(N_x, mu_init, V_val,lat_sp, Per_cos):
 list_p=[]
 for i in xrange(N_x):
  location=i*lat_sp
  mu_val=mu_init+V_val*((math.cos(2.0*math.pi*location*Per_cos)-1.0)**2.0)
  #print  location, (math.cos(2.0*math.pi*location)-1.0)**2.0, mu_val, V_val, mu_init, V_val*(((math.cos(2.0*math.pi*location)-1.0)**2.0))
  list_p.append(0.0750*abs(math.cos(2.0*math.pi*location*Per_cos)-1.0))
 return list_p


def mu_f(i, mu_0, V_0, Per_cos):
  #print 2.0*math.pi*(1.0/6.0), math.cos(2.0*math.pi*(1.0/6.0))
  #print "Potential", i, (math.cos(2.0*math.pi*i)-1.0)**2.0
  mu=mu_0+V_0*((math.cos(2.0*math.pi*i*Per_cos)-1.0)**2.0)
  return mu

def  make_H_list(N_x, g_val, d_phys, lat_sp, mu_0, mass, V_0, Per_cos):

 bdi = uni10.Bond(uni10.BD_IN, d_phys)
 bdo = uni10.Bond(uni10.BD_OUT, d_phys)
 H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heis")
 H_list=[None]*(N_x-1)

# c_i, c_i_dag, iden=C_i(d_phys)

# for i in xrange(N_x-1):
#  location=i*lat_sp
#  locationP=(i+1)*lat_sp

#  if i==0:
#   kenetic_term=uni10.otimes(c_i,c_i_dag)+uni10.otimes(c_i_dag,c_i)
#   ham =(-1.0/2.0)*(1.0/(lat_sp**2))*(mass)*kenetic_term

#   n_i=c_i_dag*c_i
#   ham =(mass)*((1.0/(lat_sp**2)))*(2.0*uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham
#   ham =(2.0*mu_f(location, mu_0, V_0, Per_cos)*uni10.otimes(n_i,iden)+mu_f(locationP, mu_0, V_0, Per_cos)*uni10.otimes(iden,n_i))*0.5+ham

#   ham =(g_val/lat_sp)*(2.0*uni10.otimes(n_i*n_i,iden)+uni10.otimes(iden,n_i*n_i))*0.5+ham
#   ham =(-1.0)*(g_val/lat_sp)*(2.0*uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham
#  elif i==N_x-2:
#   kenetic_term=uni10.otimes(c_i,c_i_dag)+uni10.otimes(c_i_dag,c_i)
#   ham =(-1.0/2.0)*(1.0/(lat_sp**2))*(mass)*kenetic_term

#   n_i=c_i_dag*c_i
#   ham =(mass)*((1.0/(lat_sp**2)))*(uni10.otimes(n_i,iden)+2.0*uni10.otimes(iden,n_i))*0.5+ham

#   ham =(mu_f(location, mu_0, V_0, Per_cos)*uni10.otimes(n_i,iden)+mu_f(locationP, mu_0, V_0, Per_cos)*2.0*uni10.otimes(iden,n_i))*0.5+ham

#   ham =(g_val/lat_sp)*(uni10.otimes(n_i*n_i,iden)+2.0*uni10.otimes(iden,n_i*n_i))*0.5+ham
#   ham =(-1.0)*(g_val/lat_sp)*(uni10.otimes(n_i,iden)+2.0*uni10.otimes(iden,n_i))*0.5+ham
#  else:
#   kenetic_term=uni10.otimes(c_i,c_i_dag)+uni10.otimes(c_i_dag,c_i)
#   ham =(-1.0/2.0)*(1.0/(lat_sp**2))*(mass)*kenetic_term

#   n_i=c_i_dag*c_i
#   ham =(mass)*((1.0/(lat_sp**2)))*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham

#   ham =(mu_f(location, mu_0, V_0, Per_cos)*uni10.otimes(n_i,iden)+mu_f(locationP, mu_0, V_0, Per_cos)*uni10.otimes(iden,n_i))*0.5+ham

#   ham =(g_val/lat_sp)*(uni10.otimes(n_i*n_i,iden)+uni10.otimes(iden,n_i*n_i))*0.5+ham
#   ham =(-1.0)*(g_val/lat_sp)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham


#  H.putBlock(ham)
#  H_list[i]=H*1.0
#  print H_list[i]

 sx = matSx()
 sy = matSy()
 sz = matSz()
 iden = matIden()
 H_list=[None]*(N_x-1)
 for i in xrange(N_x-1):
  #ham =uni10.otimes(sz,sz)+uni10.otimes(sx,sx)#+(-1.0)*uni10.otimes(sy,sy))*(0.25)
  ham=1.0
  if i==0:
   ham =-1.0*uni10.otimes(sx,sx)+(-0.50*g_val)*(uni10.otimes(iden,sz)+2.0*uni10.otimes(sz,iden))
  elif i==N_x-2:
   ham =-1.0*uni10.otimes(sx,sx)+(-0.50*g_val)*(2.0*uni10.otimes(iden,sz)+uni10.otimes(sz,iden))
  else:
   ham =-1.0*uni10.otimes(sx,sx)+(-0.50*g_val)*(uni10.otimes(iden,sz)+uni10.otimes(sz,iden))

  H.putBlock(ham)
  H_list[i]=H*1.0

 return H_list




def  particle_density_f(mps_A, Q_list,d_phys):

 mps_A.normalize()
 E_left=[None]*mps_A.N
 for i in xrange(mps_A.N):
  if i == 0:
   mps_A[i].setLabel([-1,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,2])
   E_left[0]=mps_A_dag*mps_A[i]
   E_left[0].permute([1,2],1)
   E_left[0].setLabel([-3,-4])
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
  else:
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,2])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
   E_left[i].permute([1,2],1)
   E_left[i].setLabel([-3,-4])


 E_right=[None]*mps_A.N
 for i in reversed(xrange(mps_A.N)):
  if i == 0:
   mps_A[i].setLabel([-1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,-4])
   E_right[0]=mps_A_dag*mps_A[i]*E_right[1]
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_right[i]=mps_A_dag*(mps_A[i])
   E_right[i].setLabel([-3,-4])
  else:
   mps_A[i].setLabel([1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([2,-2,-4])
   E_right[i]=mps_A_dag*(E_right[i+1]*mps_A[i])
   E_right[i].permute([1,2],1)
   E_right[i].setLabel([-3,-4])



 n_list=[None]*(2*len(Q_list))

 bdi = uni10.Bond(uni10.BD_IN, d_phys)
 bdo = uni10.Bond(uni10.BD_OUT, d_phys)
 H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heis")

 c_i,c_i_dag, iden=C_i(d_phys)
 n_i=c_i_dag*c_i
 
 
 for i in xrange(len(Q_list)):
  
  
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  ham =uni10.otimes(n_i,iden)
  H.putBlock(ham)
  H.setLabel([-1,-2,1,2])
  N_r=Q_list[i]*H*Q_t
  N_r.permute([-3,3],1)
  N_r.setLabel([-10,10])
  
  if i==0:
    mps_A[i].setLabel([-1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    val=((N_r*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i]=val[0]
  elif i==len(Q_list)-1:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-3])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*mps_A_dag)*mps_A[i])*E_left[i-1]
    n_list[2*i]=val[0]
  else:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*E_left[i-1]*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i]=val[0]
  

  
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])
  ham =uni10.otimes(iden,n_i)
  H.putBlock(ham)
  H.setLabel([-1,-2,1,2])
  N_r=Q_list[i]*H*Q_t
  N_r.permute([-3,3],1)
  N_r.setLabel([-10,10])
  
  if i==0:
    mps_A[i].setLabel([-1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    val=((N_r*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i+1]=val[0]
  elif i==len(Q_list)-1:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-3])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*mps_A_dag)*mps_A[i])*E_left[i-1]
    n_list[2*i+1]=val[0]
  else:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*E_left[i-1]*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i+1]=val[0]

 return n_list








def  kinetic_energy_f(mps_A, Q_list, d_phys,g_val,mass):

 mps_A.normalize()
 E_left=[None]*mps_A.N
 for i in xrange(mps_A.N):
  if i == 0:
   mps_A[i].setLabel([-1,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,2])
   E_left[0]=mps_A_dag*mps_A[i]
   E_left[0].permute([1,2],1)
   E_left[0].setLabel([-3,-4])
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
  else:
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,2])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
   E_left[i].permute([1,2],1)
   E_left[i].setLabel([-3,-4])


 E_right=[None]*mps_A.N
 for i in reversed(xrange(mps_A.N)):
  if i == 0:
   mps_A[i].setLabel([-1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,-4])
   E_right[0]=mps_A_dag*mps_A[i]*E_right[1]
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_right[i]=mps_A_dag*(mps_A[i])
   E_right[i].setLabel([-3,-4])
  else:
   mps_A[i].setLabel([1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([2,-2,-4])
   E_right[i]=mps_A_dag*(E_right[i+1]*mps_A[i])
   E_right[i].permute([1,2],1)
   E_right[i].setLabel([-3,-4])



 n_list=[None]*(2*len(Q_list)-1)

 bdi = uni10.Bond(uni10.BD_IN, d_phys)
 bdo = uni10.Bond(uni10.BD_OUT, d_phys)
 H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heis")

 c_i,c_i_dag, iden=C_i(d_phys)
 n_i=c_i_dag*c_i
 kenetic_term=uni10.otimes(c_i,c_i_dag)+uni10.otimes(c_i_dag,c_i)

 
 for i in xrange(len(Q_list)):
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])

  ham =(-1.0/2.0)*(mass)*kenetic_term+(mass)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))
  #ham =(g_val)*(uni10.otimes(n_i*n_i,iden)+uni10.otimes(iden,n_i*n_i))*0.5+ham
  #ham =(-1.0)*(g_val)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham

  H.putBlock(ham)
  H.setLabel([-1,-2,1,2])
  N_r=Q_list[i]*H*Q_t
  N_r.permute([-3,3],1)
  N_r.setLabel([-10,10])
  
  if i==0:
    mps_A[i].setLabel([-1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    val=((N_r*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i]=val[0]
  elif i==len(Q_list)-1:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-3])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*mps_A_dag)*mps_A[i])*E_left[i-1]
    n_list[2*i]=val[0]
  else:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*E_left[i-1]*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i]=val[0]
  

  if i==len(Q_list)-1: break
  
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([1,-2,-3])

  Q_list[i+1].setLabel([4,5,6])
  Q_tt=Q_list[i+1]*1.0
  Q_tt.setLabel([-4,5,-6])


  ham =(-1.0/2.0)*(mass)*kenetic_term+(mass)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))
  #ham =(g_val)*(uni10.otimes(n_i*n_i,iden)+uni10.otimes(iden,n_i*n_i))*0.5+ham
  #ham =(-1.0)*(g_val)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham

  H.putBlock(ham)
  H.setLabel([-2,-4,2,4])
  N_r=(Q_list[i]*H*Q_t)*(Q_list[i+1]*Q_tt)
  N_r.permute([-3,-6,3,6],2)
  N_r.setLabel([-3,-6,3,6])



  if i==0:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,-4])

    E_right[i+2].setLabel([4,-4])
    val=((N_r*mps_A_dag)*mps_A[i])*(mps_A[i+1]*mps_A_dagN)*E_right[i+2]
    n_list[2*i+1]=val[0]
    #print val
  elif i==len(Q_list)-2:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,4])

    E_left[i-1].setLabel([1,-1])
    val=((N_r*mps_A_dag*E_left[i-1])*mps_A[i])*(mps_A[i+1]*mps_A_dagN)
    n_list[2*i+1]=val[0]
    #print val
  else:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,-4])
    #print i-1, E_left[i-1] 
    E_left[i-1].setLabel([1,-1])
    E_right[i+2].setLabel([4,-4])

    val=((N_r*mps_A_dag*E_left[i-1])*mps_A[i])*(mps_A[i+1]*mps_A_dagN)*E_right[i+2]
    n_list[2*i+1]=val[0]
    #print val

 return n_list





def  Interaction_energy_f(mps_A, Q_list, d_phys,g_val,mass):

 mps_A.normalize()
 E_left=[None]*mps_A.N
 for i in xrange(mps_A.N):
  if i == 0:
   mps_A[i].setLabel([-1,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,2])
   E_left[0]=mps_A_dag*mps_A[i]
   E_left[0].permute([1,2],1)
   E_left[0].setLabel([-3,-4])
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
  else:
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,2])
   E_left[i]=mps_A_dag*(E_left[i-1]*mps_A[i])
   E_left[i].permute([1,2],1)
   E_left[i].setLabel([-3,-4])


 E_right=[None]*mps_A.N
 for i in reversed(xrange(mps_A.N)):
  if i == 0:
   mps_A[i].setLabel([-1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-1,-2,-4])
   E_right[0]=mps_A_dag*mps_A[i]*E_right[1]
  elif i == (mps_A.N-1):
   mps_A[i].setLabel([-3,-2,1])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([-4,-2,1])
   E_right[i]=mps_A_dag*(mps_A[i])
   E_right[i].setLabel([-3,-4])
  else:
   mps_A[i].setLabel([1,-2,-3])
   mps_A_dag=copy.copy(mps_A[i])
   mps_A_dag.setLabel([2,-2,-4])
   E_right[i]=mps_A_dag*(E_right[i+1]*mps_A[i])
   E_right[i].permute([1,2],1)
   E_right[i].setLabel([-3,-4])



 n_list=[None]*(2*len(Q_list)-1)

 bdi = uni10.Bond(uni10.BD_IN, d_phys)
 bdo = uni10.Bond(uni10.BD_OUT, d_phys)
 H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heis")

 c_i,c_i_dag, iden=C_i(d_phys)
 n_i=c_i_dag*c_i
 kenetic_term=uni10.otimes(c_i,c_i_dag)+uni10.otimes(c_i_dag,c_i)

 
 for i in xrange(len(Q_list)):
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([-1,-2,-3])

  #ham =(-1.0/2.0)*(mass)*kenetic_term+(mass)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))
  ham =(g_val)*(uni10.otimes(n_i*n_i,iden)+uni10.otimes(iden,n_i*n_i))*0.5
  ham =(-1.0)*(g_val)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham

  H.putBlock(ham)
  H.setLabel([-1,-2,1,2])
  N_r=Q_list[i]*H*Q_t
  N_r.permute([-3,3],1)
  N_r.setLabel([-10,10])
  
  if i==0:
    mps_A[i].setLabel([-1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    val=((N_r*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i]=val[0]
  elif i==len(Q_list)-1:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-3])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*mps_A_dag)*mps_A[i])*E_left[i-1]
    n_list[2*i]=val[0]
  else:
    mps_A[i].setLabel([1,10,-3])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-10,-4])
    E_right[i+1].setLabel([-3,-4])
    E_left[i-1].setLabel([1,-1])
    val=((N_r*E_left[i-1]*mps_A_dag)*mps_A[i])*E_right[i+1]
    n_list[2*i]=val[0]
  

  if i==len(Q_list)-1: break
  
  Q_list[i].setLabel([1,2,3])
  Q_t=Q_list[i]*1.0
  Q_t.setLabel([1,-2,-3])

  Q_list[i+1].setLabel([4,5,6])
  Q_tt=Q_list[i+1]*1.0
  Q_tt.setLabel([-4,5,-6])


  #ham =(-1.0/2.0)*(mass)*kenetic_term+(mass)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))
  ham =(g_val)*(uni10.otimes(n_i*n_i,iden)+uni10.otimes(iden,n_i*n_i))*0.5
  ham =(-1.0)*(g_val)*(uni10.otimes(n_i,iden)+uni10.otimes(iden,n_i))*0.5+ham

  H.putBlock(ham)
  H.setLabel([-2,-4,2,4])
  N_r=(Q_list[i]*H*Q_t)*(Q_list[i+1]*Q_tt)
  N_r.permute([-3,-6,3,6],2)
  N_r.setLabel([-3,-6,3,6])



  if i==0:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,-4])

    E_right[i+2].setLabel([4,-4])
    val=((N_r*mps_A_dag)*mps_A[i])*(mps_A[i+1]*mps_A_dagN)*E_right[i+2]
    n_list[2*i+1]=val[0]
    #print val
  elif i==len(Q_list)-2:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,4])

    E_left[i-1].setLabel([1,-1])
    val=((N_r*mps_A_dag*E_left[i-1])*mps_A[i])*(mps_A[i+1]*mps_A_dagN)
    n_list[2*i+1]=val[0]
    #print val
  else:
    mps_A[i].setLabel([1,3,2])
    mps_A_dag=copy.copy(mps_A[i])
    mps_A_dag.setLabel([-1,-3,-2])

    mps_A[i+1].setLabel([2,6,4])
    mps_A_dagN=copy.copy(mps_A[i+1])
    mps_A_dagN.setLabel([-2,-6,-4])
    #print i-1, E_left[i-1] 
    E_left[i-1].setLabel([1,-1])
    E_right[i+2].setLabel([4,-4])

    val=((N_r*mps_A_dag*E_left[i-1])*mps_A[i])*(mps_A[i+1]*mps_A_dagN)*E_right[i+2]
    n_list[2*i+1]=val[0]
    #print val


 n_listA=[ 1.0*n_list[i] for i in xrange(len(n_list))  ]
 return n_listA












def Store_mps(MPS_A, N_x):
 for i in xrange(N_x):
   MPS_A[i].save("StoreMPS/mps" + str(i))

def Reload_mps(MPS_A, N_x):
 for i in xrange(N_x):
   MPS_A[i]=uni10.UniTensor("StoreMPS/mps" + str(i))


def Store_Q_list(Q_list, N_x):
 for i in xrange(N_x):
   Q_list[i].save("StoreQ/Q" + str(i))

def Reload_Q_list(Q_list, N_x):
 for i in xrange(N_x):
   Q_list[i]=uni10.UniTensor("StoreQ/Q" + str(i))









def MaxAbs(c):
 blk_qnums = c.blockQnum()
 max_list=[]
 for qnum in blk_qnums:
    c_mat=c.getBlock(qnum)
    max_list.append(c_mat.absMax())
 max_list_f=[abs(x) for x in max_list]
 return max(max_list_f)

def max_ten(a):
 Max_val=MaxAbs(a)
 if ( Max_val < 0.5e-1) or (Max_val > 0.5e+1)   :

  if Max_val >= 1:
   print ">1",Max_val
   a=a*(1.00/Max_val)
  if Max_val < 1: 
   print "<1",Max_val
   a=a*(1.00/Max_val)

#  if Max_val >= 1:
#   print ">1",Max_val, Max_val**(1./2.)
#   a=a*(1.00/(Max_val**(1./2.)))
#  if Max_val < 1: 
#   print "<1",Max_val
#   a=a*(1.00/Max_val)

 else: a=a;
 return a



def Mat_uni_to_np(Mat_uni):
 dim0=int(Mat_uni.row())
 dim1=int(Mat_uni.col())
 Mat_np=np.zeros((dim0,dim1))
 for i in xrange(dim0):
  for j in xrange(dim1):
   Mat_np[i,j]=Mat_uni[i*dim1+j]
 return  Mat_np











































def sqrt_general(N2):
  N_init=copy.copy(N2)
  blk_qnums = N2.blockQnum()
  for qnum in blk_qnums:
   M=N2.getBlock(qnum)
   eig=M.eigh()
   
   e=Sqrt_mat(eig[0])
   U_trans=copy.copy(eig[1])
   U_trans.transpose()
   M=U_trans*e*eig[1]
   N_init.putBlock(qnum,M)
  return N_init
def Sqrt_mat(e):
 d=int(e.row())
 
 for q in xrange(d):
   ##print e[q] 
   if e[q] > 0:  
    e[q]=((e[q])**(1.00/2.00))
   else:  
    e[q]=0.0 
 return e  
 









def matSx():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=(1.0)*uni10.Matrix(dim, dim, [0.0, 1.0, 1.00, 0.0])
  return Mat 

def matSz():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=(1.0)*uni10.Matrix(dim, dim, [1.0, 0, 0, -1.0]);
  return Mat 

def matSy():
  spin = 0.5
  dim = int(spin * 2 + 1)
  Mat=(1.0)*uni10.Matrix(dim, dim, [0.0, -1.00, 1.00, 0.00]);
  return Mat 


def matIden():
    spin_t=0.5
    dimT = int(2*spin_t + 1)
    Mat=uni10.Matrix(dimT, dimT,[1,0,0,1])
    return Mat



def Heisenberg(h, d_phys, Model):

    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heisenberg")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
    if Model=="ITF":
     ham =h[1]*uni10.otimes(sz,sz)*(-1)+(-0.2500)*h[0]*(uni10.otimes(iden,sx)+uni10.otimes(sx,iden))
    if Model=="Heis":
     ham =(h[0]*uni10.otimes(sz,sz)+h[1]*uni10.otimes(sx,sx)+(-1.0*h[1])*uni10.otimes(sy,sy))*(0.25)

    #print ham
    H.putBlock(ham)
    return H



def Heisenberg0(h, d_phys, Model):

    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heisenberg")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
    if Model=="ITF":
     ham =h[1]*uni10.otimes(sz,sz)*(-1)+(-0.2500)*float(h[0])*(uni10.otimes(iden,sx)+2.00*uni10.otimes(sx,iden))
    if Model=="Heis":
     ham =(h[0]*uni10.otimes(sz,sz)+h[1]*uni10.otimes(sx,sx)+(-1.0*h[1])*uni10.otimes(sy,sy))*(0.25)

    #print ham
    H.putBlock(ham)
    return H



def HeisenbergN(h, d_phys, Model):

    bdi = uni10.Bond(uni10.BD_IN, d_phys)
    bdo = uni10.Bond(uni10.BD_OUT, d_phys)
    H = uni10.UniTensor([bdi, bdi, bdo, bdo], "Heisenberg")
    sx = matSx()
    sy = matSy()
    sz = matSz()
    iden = matIden()
    #ham=uni10.otimes(sz,sz)+uni10.otimes(sx,sx)+(-1.0)*uni10.otimes(sy,sy)
    if Model=="ITF":
     ham =h[1]*uni10.otimes(sz,sz)*(-1)+(-0.2500)*h[0]*(2.00*uni10.otimes(iden,sx)+uni10.otimes(sx,iden))
    if Model=="Heis":
     ham =(h[0]*uni10.otimes(sz,sz)+h[1]*uni10.otimes(sx,sx)+(-1.0*h[1])*uni10.otimes(sy,sy))*(0.25)

    #print ham
    H.putBlock(ham)
    return H



























def inverse(Landa2):
 invLanda2=uni10.UniTensor(Landa2.bond())
 blk_qnums=Landa2.blockQnum()
 for qnum in blk_qnums:
  D=int(Landa2.getBlock(qnum).row())
  D1=int(Landa2.getBlock(qnum).col())
  invL2 = uni10.Matrix(D, D1,True)
  invLt = uni10.Matrix(D, D1,True)
  invLt=Landa2.getBlock(qnum,True)
  #print invLt[0], invLt[1], invLt[2], invLt[3]
  for i in xrange(D):
      invL2[i] = 0 if ((invLt[i].real) < 1.0e-10) else (1.00 / (invLt[i].real))

  invLanda2.putBlock(qnum,invL2)
 return invLanda2


#########  prerequisite functions  #############
def   setTruncation1(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1),theta.bond(2),bdo_mid])
    GB.assign([bdi_mid,theta.bond(3),theta.bond(4),theta.bond(5)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
def   setTruncationMPS(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_mergemps(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0), theta.bond(1),bdo_mid])
    GB.assign([bdi_mid,  theta.bond(2)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA

def   sv_mergemps(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs











def   sv_merge1(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs



    
    
    
  
  
 
 

def svd_parityrl(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(3).Qlist())
    bd2=uni10.Bond(uni10.BD_IN,theta.bond(4).Qlist())
    bd3=uni10.Bond(uni10.BD_IN,theta.bond(5).Qlist())

    GA=uni10.UniTensor([theta.bond(0),theta.bond(1),theta.bond(2),theta.bond(3),theta.bond(4),theta.bond(5)])
    LA=uni10.UniTensor([bd1,bd2,bd3,theta.bond(3),theta.bond(4),theta.bond(5)])
    GB=uni10.UniTensor([bd1,bd2,bd3,theta.bond(3),theta.bond(4),theta.bond(5)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB

def svd_parity(theta):

    bd1=uni10.Bond(uni10.BD_IN,theta.bond(1).Qlist())

    GA=uni10.UniTensor([theta.bond(0),theta.bond(1)])
    LA=uni10.UniTensor([bd1,theta.bond(1)])
    GB=uni10.UniTensor([bd1,theta.bond(1)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


def svd_parity1(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim())
    #print bdi1
    GA=uni10.UniTensor([theta.bond(0),theta.bond(1), bdo1])
    LA=uni10.UniTensor([bdi1,bdo1])
    GB=uni10.UniTensor([bdi1,theta.bond(2),theta.bond(3)])

    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        GA.putBlock(qnum, svds[qnum][0])
        LA.putBlock(qnum, svds[qnum][1])
        GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


def svd_parity2(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(3).dim()*theta.bond(4).dim()*theta.bond(5).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(3).dim()*theta.bond(4).dim()*theta.bond(5).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())

    if bdi.dim()<=bdi1.dim():
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(3),theta.bond(4), theta.bond(5)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(3),theta.bond(4), theta.bond(5)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB




def svd_parity5(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(3).dim()*theta.bond(4).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(3).dim()*theta.bond(4).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim())

    if bdi.dim()<=bdi1.dim():
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB


def svd_parity6(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim())

    if bdi.dim()<=bdi1.dim():
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(2),theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(2),theta.bond(3),theta.bond(4)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB







def setTruncation2(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3), bdo_mid])
    GB.assign([bdi_mid, theta.bond(4), theta.bond(5), theta.bond(6), theta.bond(7)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA


def setTruncation3(theta, chi):
    LA=uni10.UniTensor(theta.bond())
    GA=uni10.UniTensor(theta.bond())
    GB=uni10.UniTensor(theta.bond())
    svds = {}
    blk_qnums = theta.blockQnum()
    dim_svd=[]
    for qnum in blk_qnums:
        svds[qnum] = theta.getBlock(qnum).svd()
        dim_svd.append(int(svds[qnum][1].col()))
    svs = []
    bidxs = []
    for bidx in xrange(len(blk_qnums)):
        svs, bidxs = sv_merge1(svs, bidxs, bidx, svds[blk_qnums[bidx]][1], chi,len(blk_qnums))
    dims = [0] * len(blk_qnums)
    for bidx in bidxs:
        dims[bidx] += 1  
    qnums = []
    for bidx in xrange(len(blk_qnums)):
        qnums += [blk_qnums[bidx]] * dims[bidx]
    bdi_mid = uni10.Bond(uni10.BD_IN, qnums)
    bdo_mid = uni10.Bond(uni10.BD_OUT, qnums)
    GA.assign([theta.bond(0),theta.bond(1), bdo_mid])
    GB.assign([bdi_mid, theta.bond(2), theta.bond(3)])
    LA.assign([bdi_mid, bdo_mid])
    degs = bdi_mid.degeneracy()
    for qnum, dim in degs.iteritems():
        if qnum not in svds:
            raise Exception("In setTruncaton(): Fatal error.")
        svd = svds[qnum]
        GA.putBlock(qnum, svd[0].resize(svd[0].row(), dim))
        GB.putBlock(qnum, svd[2].resize(dim, svd[2].col()))
        LA.putBlock(qnum, svd[1].resize(dim, dim)  )
    return GA, GB, LA

def sv_merge1(svs, bidxs, bidx, sv_mat, chi, len_qn):
    if(len(svs)):
        length = len(svs) + sv_mat.elemNum()
        length = length if length < chi else chi
        ori_svs = svs
        ori_bidxs = bidxs
        svs = [0] * length
        bidxs = [0] * length
        svs = []
        bidxs = []
        cnt  = 0
        cur1 = 0
        cur2 = 0
        while cnt < length:
            if(cur1 < len(ori_svs)) and cur2 < sv_mat.elemNum():
                if ori_svs[cur1] >= sv_mat[cur2]:
                    if (ori_svs[cur1] > -0.01):
                     svs.append(ori_svs[cur1]) 
                     bidxs.append(ori_bidxs[cur1])
                    cur1 += 1
                else:
                    if (sv_mat[cur2] > -0.01):
                     svs.append( sv_mat[cur2])
                     bidxs.append(bidx) 
                    cur2 += 1
            elif cur2 < sv_mat.elemNum() :
                for i in xrange(cnt, length):
                    if (sv_mat[cur2] > -0.01):
                     svs.append(sv_mat[cur2]) 
                     bidxs.append(bidx) 
                    cur2 += 1
                break
            else:
                for i in xrange(cur1, len(ori_svs)):
                 svs.append(ori_svs[i])
                 bidxs.append(ori_bidxs[i]) 
                break
            cnt += 1
    else:
       if (len_qn is 1):
        bidxs = [bidx] * chi  
        svs = [sv_mat[i] for i in xrange(chi)]
       elif (sv_mat[0] > -0.01):
        bidxs = [bidx] * sv_mat.elemNum()
        svs = [sv_mat[i] for i in xrange(sv_mat.elemNum())]
       else: bidxs = [bidx];  svs = [sv_mat[0]];  
    return svs, bidxs


def svd_parity3(theta):

    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(4).dim()*theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(4).dim()*theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim())

    if bdi.dim()<=bdi1.dim():
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(4),theta.bond(5), theta.bond(6),theta.bond(7)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(4),theta.bond(5), theta.bond(6),theta.bond(7)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB

def svd_parity4(theta):
    bdi1=uni10.Bond(uni10.BD_IN,theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim()*theta.bond(8).dim()*theta.bond(9).dim())
    bdo1=uni10.Bond(uni10.BD_OUT,theta.bond(5).dim()*theta.bond(6).dim()*theta.bond(7).dim()*theta.bond(8).dim()*theta.bond(9).dim())

    bdi=uni10.Bond(uni10.BD_IN,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())
    bdo=uni10.Bond(uni10.BD_OUT,theta.bond(0).dim()*theta.bond(1).dim()*theta.bond(2).dim()*theta.bond(3).dim()*theta.bond(4).dim())

    if bdi.dim()<=bdi1.dim():
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3),theta.bond(4), bdo])
     LA=uni10.UniTensor([bdi,bdo])
     GB=uni10.UniTensor([bdi,theta.bond(5),theta.bond(6), theta.bond(7),theta.bond(8),theta.bond(9)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]

     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])
    else:
     #print theta.printDiagram()
     GA=uni10.UniTensor([theta.bond(0),theta.bond(1), theta.bond(2),theta.bond(3),theta.bond(4), bdo1])
     LA=uni10.UniTensor([bdi1,bdo1])
     GB=uni10.UniTensor([bdi1,theta.bond(5),theta.bond(6), theta.bond(7),theta.bond(8),theta.bond(9)])

     svds = {}
     blk_qnums = theta.blockQnum()
     dim_svd=[]
     for qnum in blk_qnums:
         svds[qnum] = theta.getBlock(qnum).svd()
         #print   svds[qnum][0].row(), svds[qnum][0].col()
         GA.putBlock(qnum, svds[qnum][0])
         LA.putBlock(qnum, svds[qnum][1])
         GB.putBlock(qnum, svds[qnum][2])

    return GA, LA, GB



def Sqrt(Landa):
  Landa_cp=copy.copy(Landa)
  blk_qnums=Landa.blockQnum()
  for qnum in blk_qnums:
   D=int(Landa_cp.getBlock(qnum).col())
   Landa_cpm=Landa_cp.getBlock(qnum)
   Landam=Landa_cp.getBlock(qnum)
   for i in xrange(D):
    for j in xrange(D):
     if Landam[i*D+j] > 1.0e-12:
      Landa_cpm[i*D+j]=Landam[i*D+j]**(1.00/2.00)
     else:
      Landa_cpm[i*D+j]=0
   Landa_cp.putBlock(qnum,Landa_cpm)
  return Landa_cp 

